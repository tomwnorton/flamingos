#ifndef FLAMINGOS_STRING_H
#define FLAMINGOS_STRING_H

#include <stddef.h>
#include "cxx.h"

EXTERN_C size_t strlen(const char *s);
EXTERN_C int strncmp(const char *lhs, const char *rhs, unsigned int count);

#endif
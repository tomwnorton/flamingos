.section .text

/**
 * \brief Output a single byte to the given port
 *
 * \param portNumber The port number to which the byte is sent
 * \param byte       The byte to send to the aforementioned port
 *
 * \returns The byte that was written
 */
.globl f_outb
.type f_outb, @function
f_outb:
    movl $0, %eax
    movw 4(%esp), %dx
    movb 8(%esp), %al
    outb %al, %dx
    ret

/**
 * \brief Output a string of bytes to the given port
 *
 * I could have used REP OUTSB instructions instead of doing this manually.
 * I *think* it would work, as we are now using a flat kernel, but I would
 * like to make sure I have tested that this is working first before I
 * refactor to something else.
 *
 * \param portNumber 16-bit port number to which the data should be sent
 * \param data       32-bit address of the data that should be sent to the given port
 * \param length     The 32-bit number of bytes to send
 */
.section .text
.globl f_out_bytes
.type f_out_bytes, @function
f_out_bytes:
    pushl %ebp
    movl %esp, %ebp
    pushl %ebx
    pushl %esi
    movl $0, %esi
    movw 8(%ebp), %dx
    movl 12(%ebp), %ebx
    movl 16(%ebp), %ecx
    cmpl $0, %ecx
    je end_loop
start_loop:
    movb (%ebx,%esi, 1), %al
    outb %al, %dx
    incl %esi
    loop start_loop /* The loop instruction automatically compares and decrements the ecx register */
end_loop:
    popl %ebx
    popl %esi
    popl %ebp
    ret

/**
 * \brief Retrieve a byte from a port
 *
 * \param portNumber The port number from which to retrieve the byte
 * \returns          The byte retrieved from the port
 */
.globl f_inb
.type f_inb, @function
f_inb:
    movl $0, %eax
    movw 4(%esp), %dx
    inb %dx, %al
    ret
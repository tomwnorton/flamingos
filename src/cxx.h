#ifndef __CXX_H__
#define __CXX_H__

#ifdef __cplusplus
#define EXTERN_C extern "C"
#define EXTERN_C_START extern "C" {
#define EXTERN_C_END }
#else
#define EXTERN_C
#define EXTERN_C_START
#define EXTERN_C_END
#endif

#endif // __CXX_H__
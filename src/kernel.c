#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include "cxx.h"
#include "gdt.h"
#include "serial.h"
#include "log.h"
#include "multiboot.h"

#if defined(__linux__) || !defined(__i386__)
#error "This kernel is designed for the i686-elf cross compiler."
#endif

enum vga_color {
    VGA_COLOR_BLACK,
    VGA_COLOR_BLUE,
    VGA_COLOR_GREEN,
    VGA_COLOR_CYAN,
    VGA_COLOR_RED,
    VGA_COLOR_MAGENTA,
    VGA_COLOR_BROWN,
    VGA_COLOR_LIGHT_GREY,
    VGA_COLOR_DARK_GREY,
    VGA_COLOR_LIGHT_BLUE,
    VGA_COLOR_LIGHT_GREEN,
    VGA_COLOR_LIGHT_CYAN,
    VGA_COLOR_LIGHT_RED,
    VGA_COLOR_LIGHT_MAGENTA,
    VGA_COLOR_LIGHT_BROWN,
    VGA_COLOR_WHITE
};

static inline uint8_t vga_entry_color(enum vga_color fg, enum vga_color bg) {
    return fg | bg << 4;
}

static inline uint16_t vga_entry(unsigned char c, uint8_t color) {
    return (uint16_t) c | (uint16_t) color << 8;
}

static inline size_t strlne(const char *str) {
    size_t len = 0UL;
    while (str[len]) {
        ++len;
    }
    return len;
}

static const size_t VGA_WIDTH = 80UL;
static const size_t VGA_HEIGHT = 25UL;

size_t terminal_row;
size_t terminal_column;
size_t terminal_color;
uint16_t *terminal_buffer;

void terminal_initialize(void) {
    terminal_row = 0;
    terminal_column = 0;
    terminal_color = vga_entry_color(VGA_COLOR_LIGHT_GREY, VGA_COLOR_BLACK);
    terminal_buffer = (uint16_t *) 0xB8000;
    for (size_t y = 0; y < VGA_HEIGHT; ++y) {
        for (size_t x = 0; x < VGA_WIDTH; ++x) {
            const size_t index = y * VGA_WIDTH + x;
            terminal_buffer[index] = vga_entry(' ', terminal_color);
        }
    }
}

void terminal_setcolor(uint8_t color) {
    terminal_color = color;
}

void terminal_putentryat(char c, uint8_t color, size_t x, size_t y) {
    const size_t index = y * VGA_WIDTH + x;
    terminal_buffer[index] = vga_entry(c, color);
}

void terminal_putchar(char c) {
    terminal_putentryat(c, terminal_color, terminal_column, terminal_row);
    if (++terminal_column == VGA_WIDTH) {
        terminal_column = 0;
        if (++terminal_row == VGA_HEIGHT) {
            terminal_row = 0;
        }
    }
}

void terminal_write(const char *data, size_t size) {
    for (size_t i = 0; i < size; ++i) {
        terminal_putchar(data[i]);
    }
}

void terminal_writestring(const char* data) {
    while (*data != '\0') {
        terminal_putchar(*data);
        ++data;
    }
}

void terminal_writehex(unsigned int hex) {
    const char * const DIGITS = "0123456789ABCDEF";
    char output[9UL];
    char *pos = output + 7;
    output[8UL] = '\0';

    do {
        char c = DIGITS[hex % 0x10];
        *pos = c;
        --pos;
        hex /= 0x10;
    } while (hex != 0);
    terminal_writestring(++pos);
}

static int log_callback(const char *s, int count, bool finished) {
    for (int i = 0; i < count; ++i) {
        f_serial_write_byte(s[i]);
    }
    if (finished) {
        f_serial_write_string("\n");
    }
    return count;
}

extern void *F_PROGRAM_END;
extern void *F_MAPPED_KERNEL_ADDRESS;
extern void *F_TEXT_START;
extern void *F_REAL_KERNEL_START;
extern void *F_HEAP_START;

static inline const char *multiboot_feature_included(struct MultibootInfo *multibootInfo, int flag) {
    return (multibootInfo->flags & flag) ? "Yes" : "No";
}

#define F_MULTIBOOT_FEATURE_INCLUDED(flag) multiboot_feature_included(multibootInfo, F_MULTIBOOT_FLAG_##flag)

static bool verify_multiboot(unsigned int flags) {
    bool result = true;
    if (!(flags & F_MULTIBOOT_FLAG_MEMORY)) {
        F_LOG_FATAL("Memory not included in multiboot info");
        result = false;
    }
    if (!(flags & F_MULTIBOOT_FLAG_MEMORY_MAP)) {
        F_LOG_FATAL("Memory map not included in multiboot info");
        result = false;
    }
    if (!(flags & F_MULTIBOOT_FLAG_BOOT_LOADER_NAME)) {
        F_LOG_FATAL("Boot loader name not included in multiboot info");
        result = false;
    }
    if (!(flags & F_MULTIBOOT_FLAG_VESA_BIOS_EXTENSIONS)) {
        F_LOG_FATAL("VBE support not included in multiboot info");
        result = false;
    }
    return result;
}

void kernel_main(struct MultibootInfo *multibootInfo) {
    size_t mappedKernelAddress = (size_t) &F_MAPPED_KERNEL_ADDRESS;
    size_t realKernelStart = (size_t) &F_REAL_KERNEL_START;
    size_t startAddress = (size_t) &F_TEXT_START;
    size_t programEnd = (size_t) &F_PROGRAM_END;
    size_t heapStartMapped = (size_t) &F_HEAP_START;
    size_t heapStartPhysical = heapStartMapped - mappedKernelAddress + realKernelStart;
    ptrdiff_t programSize = programEnd - (startAddress - realKernelStart + mappedKernelAddress);
    ptrdiff_t programSizeKiB = programSize / 1024;

    f_log_callback(log_callback);
    terminal_initialize();
    terminal_writestring("Hello, kernel world!");
    F_LOG_INFO("The kernel is running");
    F_LOG_INFO("testing %s %s!", "variadic", "parameters");
    F_LOG_INFO("testing long long parameters %llu", 5000000000ULL);
    F_LOG_INFO("Testing hex upper: %X, %lX, %llX", 0xABCDU, 0xDCBA, 0x123456789ABCDEFULL);
    F_LOG_INFO("Testing unsigned decimals: %u, %u, %u", 12345, 54321, 3000000000U);
    F_LOG_INFO("Testing signed decimals: %d, %d, %d", 123, 456, -123);
    F_LOG_INFO("Physical Start Address:  %8lX", realKernelStart);
    F_LOG_INFO("Should be 1M:            %8lX", startAddress - realKernelStart + mappedKernelAddress);
    F_LOG_INFO("Kernel end address:      %8lX", programEnd);
    F_LOG_INFO("Heap Start:              %8lX", heapStartMapped);
    F_LOG_INFO("Heap Start Physical:     %8lX", heapStartPhysical);
    F_LOG_INFO("Kernel size:   %d bytes, %d KiB", programSize, programSizeKiB);
    F_LOG_INFO("Multiboot Information:");
    F_LOG_INFO("  Address: %X", multibootInfo);
    F_LOG_INFO("  Memory: %s", F_MULTIBOOT_FEATURE_INCLUDED(MEMORY));
    F_LOG_INFO("  Boot Device: %s", F_MULTIBOOT_FEATURE_INCLUDED(BOOT_DEVICE));
    F_LOG_INFO("  Command Line: %s", F_MULTIBOOT_FEATURE_INCLUDED(CMDLINE));
    F_LOG_INFO("  a.out Symbols: %s", F_MULTIBOOT_FEATURE_INCLUDED(SYMBOLS_A_OUT));
    F_LOG_INFO("  ELF Symbols: %s", F_MULTIBOOT_FEATURE_INCLUDED(SYMBOLS_ELF));
    F_LOG_INFO("  Memory Map: %s", F_MULTIBOOT_FEATURE_INCLUDED(MEMORY_MAP));
    F_LOG_INFO("  Drives: %s", F_MULTIBOOT_FEATURE_INCLUDED(DRIVES));
    F_LOG_INFO("  Config Table: %s", F_MULTIBOOT_FEATURE_INCLUDED(CONFIG_TABLE));
    F_LOG_INFO("  Boot Loader Name: %s", F_MULTIBOOT_FEATURE_INCLUDED(BOOT_LOADER_NAME));
    F_LOG_INFO("  APM TABLE: %s", F_MULTIBOOT_FEATURE_INCLUDED(APM_TABLE));
    F_LOG_INFO("  VBE: %s", F_MULTIBOOT_FEATURE_INCLUDED(VESA_BIOS_EXTENSIONS));
    if (!verify_multiboot(multibootInfo->flags)) {
        return;
    }
    F_LOG_INFO("Boot Loader Name: %s", multibootInfo->boot_loader_name);
    F_LOG_INFO("Memory Map Address: %X", multibootInfo->memory_map_entries);
    F_LOG_INFO("Memory Map Length: %u", multibootInfo->memory_map_length);
    F_LOG_INFO("Memory Map End Address: %X", ((size_t) multibootInfo->memory_map_entries) + multibootInfo->memory_map_length);
    F_LOG_INFO("VBE Control Info Address: %X", multibootInfo->vbe_control_info);
    F_LOG_INFO("VBE Mode Info Address: %X", multibootInfo->vbe_mode_info);

    char *memoryMapEndAddr = ((char *)multibootInfo->memory_map_entries) + multibootInfo->memory_map_length;
    F_LOG_INFO("Memory Map End Addr: %X", memoryMapEndAddr);
    struct MultibootMemoryMapEntry *entry = multibootInfo->memory_map_entries;
    int index = 0;
    F_LOG_INFO("|   Base Address   |      Length      | Type |");
    F_LOG_INFO("|------------------|------------------|------|");
    while (entry < memoryMapEndAddr) {
        F_LOG_INFO("| %08X%08X | %08X%08X | %4u |",
                   entry->base_addr_high,
                   entry->base_addr_low,
                   entry->length_high,
                   entry->length_low,
                   entry->type);
        ++index;
        ++entry;
    }
}
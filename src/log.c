#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>
#include "cxx.h"
#include "string.h"
#include "log.h"

// For some reason I can't get the linker to see the f_abort in abort.c for tests
// Maybe I'll notice later, but in the meantime, here's the work-around.
#ifdef TEST_RUN
    #include <stdlib.h>
    #define f_abort abort
#else
    void f_abort();
#endif

const char *F_LOG_LEVELS[] = {
    "TRACE",
    "DEBUG",
    "INFO ",
    "WARN ",
    "ERROR",
    "FATAL"
};

static int f_log_noop(__attribute__((unused)) const char *s, int count, __attribute__((unused)) bool finished) {
    return count;
}

FLogCallback g_callback = f_log_noop;

static const char * const DIGITS = "0123456789";
static const char * const HEX_LOWERCASE_DIGITS = "0123456789abcdef";
static const char * const HEX_UPPERCASE_DIGITS = "0123456789ABCDEF";
static const int MAX_DEC_DIGITS = 20;
static const int MAX_HEX_DIGITS = 16;

static inline void f_log_level(int logLevel) {
    g_callback(F_LOG_LEVELS[logLevel], 5, false);
    g_callback(" ", 1, false);
}

static void f_log_udec(uint64_t number, int width, const char *paddingCharacter, bool useLeftAlignment) {
    char buffer[MAX_DEC_DIGITS + 1];
    buffer[MAX_DEC_DIGITS] = '\0';
    char *pos = buffer + MAX_DEC_DIGITS;
    do {
        int digit = number % 10;
        char c = DIGITS[digit];
        --pos;
        *pos = c;
        number /= 10;
    } while (number != 0);
    int digitCount = buffer - pos + MAX_DEC_DIGITS;
    if (useLeftAlignment) {
        g_callback(pos, digitCount, false);
    }
    for (int i = digitCount; i < width; ++i) {
        g_callback(useLeftAlignment ? " " : paddingCharacter, 1, false);
    }
    if (!useLeftAlignment) {
        g_callback(pos, digitCount, false);
    }
}

static void f_log_uhex(uint64_t number, const char * const allDigits, int width, const char *paddingCharacter, bool useLeftAlignment) {
    char buffer[MAX_HEX_DIGITS + 1];
    buffer[MAX_HEX_DIGITS] = '\0';
    char *pos = buffer + MAX_HEX_DIGITS;
    do {
        int digit = number % 0x10;
        char c = allDigits[digit];
        --pos;
        *pos = c;
        number /= 0x10;
    } while (number != 0);
    
    int digitCount = buffer - pos + MAX_HEX_DIGITS;
    if (useLeftAlignment) {
        g_callback(pos, digitCount, false);
    }
    for (int i = digitCount; i < width; ++i) {
        g_callback(useLeftAlignment ? " " : paddingCharacter, 1, false);
    }
    if (!useLeftAlignment) {
        g_callback(pos, digitCount, false);
    }
}

static inline void f_log_uhex_lower(uint64_t number, int width, const char *paddingCharacter, bool useLeftAlignment) {
    f_log_uhex(number, HEX_LOWERCASE_DIGITS, width, paddingCharacter, useLeftAlignment);
}

static inline void f_log_uhex_upper(uint64_t number, int width, const char *paddingCharacter, bool useLeftAlignment) {
    f_log_uhex(number, HEX_UPPERCASE_DIGITS, width, paddingCharacter, useLeftAlignment);
}

static void f_log_sdec(int64_t number, int width, const char *paddingCharacter, bool useLeftAlignment) {
    bool negative = number < 0;
    if (negative) {
        number = -number;
    }
    char buffer[MAX_DEC_DIGITS + 1];
    buffer[MAX_DEC_DIGITS] = '\0';
    char *pos = buffer + MAX_DEC_DIGITS;
    do {
        int digit = number % 10;
        char c = DIGITS[digit];
        --pos;
        *pos = c;
        number /= 10;
    } while (number != 0);
    int digitCount = buffer - pos + MAX_DEC_DIGITS;
    if (useLeftAlignment) {
        if (negative) {
            g_callback("-", 1, false);
        }
        g_callback(pos, digitCount, false);
        for (int i = negative ? digitCount + 1 : digitCount; i < width; ++i) {
            g_callback(" ", 1, false);
        }
    } else {
        if (negative) {
            if (*paddingCharacter == '0') {
                g_callback("-", 1, false);
                for (int i = digitCount + 1; i < width; ++i) {
                    g_callback("0", 1, false);
                }
            } else {
                for (int i = digitCount + 1; i < width; ++i) {
                    g_callback(" ", 1, false);
                }
                g_callback("-", 1, false);
            }
        } else {
            for (int i = digitCount; i < width; ++i) {
                g_callback(paddingCharacter, 1, false);
            }
        }
        g_callback(pos, digitCount, false);
    }
}

enum State {
    STATE_NORMAL,
    STATE_NEW,
    STATE_WIDTH_1,
    STATE_WIDTH_2,
    STATE_STRING,
    STATE_UNSIGNED_DECIMAL,
    STATE_SIGNED_DECIMAL,
    STATE_HEX_LOWER,
    STATE_HEX_UPPER,
    STATE_LONG,
    STATE_LONG_DECIMAL,
    STATE_LONG_HEX_LOWER,
    STATE_LONG_HEX_UPPER,
    STATE_LONG_LONG,
    STATE_LONG_LONG_DECIMAL,
    STATE_LONG_LONG_HEX_LOWER,
    STATE_LONG_LONG_HEX_UPPER,
    STATE_PERCENT,
    STATE_INVALID
};

struct Parser {
    enum State state;
    unsigned int width;
    int parameterCharCount;
    char parameterBuffer[32];
    char paddingCharacter;
    char c;
    bool useLeftAlignment;
};

typedef void (*Action)(struct Parser *parser, va_list *args);

struct Transition {
    enum State begin_state;
    enum State end_state;
    int c;
    Action action;
};

static void Parser_init(struct Parser *parser) {
    parser->width = 0U;
    parser->parameterCharCount = 0;
    parser->state = STATE_NORMAL;
    parser->paddingCharacter = ' ';
    parser->useLeftAlignment = false;
}

static void Parser_normal(struct Parser *parser, __attribute__((unused)) va_list *args) {
    g_callback(&parser->c, 1, false);
}

static void Parser_new(struct Parser *parser, __attribute__((unused)) va_list *args) {
    parser->parameterBuffer[0] = '%';
    parser->parameterCharCount = 1;
    parser->width = 0;
    parser->paddingCharacter = ' ';
    parser->useLeftAlignment = false;
}

static void Parser_string_parameter(struct Parser *parser, va_list *args) {
    char *param = va_arg(*args, char *);
    size_t length = strlen(param);
    if (parser->useLeftAlignment) {
        g_callback(param, length, false);
        if (length < parser->width) {
            for (size_t i = length; i < parser->width; ++i) {
                g_callback(" ", 1, false);
            }
        }
    } else {
        if (length < parser->width) {
            for (size_t i = length; i < parser->width; ++i) {
                g_callback(" ", 1, false);
            }
        }
        g_callback(param, length, false);
    }
}

static void Parser_unsigned_decimal_parameter(__attribute__((unused)) struct Parser *parser, va_list *args) {
    uint32_t param = va_arg(*args, uint32_t);
    f_log_udec(param, parser->width, &parser->paddingCharacter, parser->useLeftAlignment);
}

static void Parser_unsigned_long_decimal_parameter(__attribute__((unused)) struct Parser *parser, va_list *args) {
    uint32_t param = va_arg(*args, uint32_t);
    f_log_udec(param, parser->width, &parser->paddingCharacter, parser->useLeftAlignment);
}

static void Parser_unsigned_long_long_decimal_parameter(__attribute__((unused)) struct Parser *parser, va_list *args) {
    uint64_t param = va_arg(*args, uint64_t);
    f_log_udec(param, parser->width, &parser->paddingCharacter, parser->useLeftAlignment);
}

static void Parser_signed_decimal_parameter(__attribute__((unused)) struct Parser *parser, va_list *args) {
    int32_t param = va_arg(*args, int32_t);
    f_log_sdec(param, parser->width, &parser->paddingCharacter, parser->useLeftAlignment);
}

static void Parser_signed_long_decimal_parameter(__attribute__((unused)) struct Parser *parser, va_list *args) {
    int32_t param = va_arg(*args, int32_t);
    f_log_sdec(param, parser->width, &parser->paddingCharacter, parser->useLeftAlignment);
}

static void Parser_signed_long_long_decimal_parameter(__attribute__((unused)) struct Parser *parser, va_list *args) {
    long long param = va_arg(*args, long long);
    f_log_sdec(param, parser->width, &parser->paddingCharacter, parser->useLeftAlignment);
}

static void Parser_hex_lower_parameter(__attribute__((unused)) struct Parser *parser, va_list *args) {
    uint32_t param = va_arg(*args, uint32_t);
    f_log_uhex_lower(param, parser->width, &parser->paddingCharacter, parser->useLeftAlignment);
}

static void Parser_unsigned_long_hex_lower_parameter(__attribute__((unused)) struct Parser *parser, va_list *args) {
    uint32_t param = va_arg(*args, uint32_t);
    f_log_uhex_lower(param, parser->width, &parser->paddingCharacter, parser->useLeftAlignment);
}

static void Parser_unsigned_long_long_hex_lower_parameter(__attribute__((unused)) struct Parser *parser, va_list *args) {
    uint64_t param = va_arg(*args, uint64_t);
    f_log_uhex_lower(param, parser->width, &parser->paddingCharacter, parser->useLeftAlignment);
}

static void Parser_hex_upper_parameter(__attribute__((unused)) struct Parser *parser, va_list *args) {
    uint32_t param = va_arg(*args, uint32_t);
    f_log_uhex_upper(param, parser->width, &parser->paddingCharacter, parser->useLeftAlignment);
}

static void Parser_unsigned_long_hex_upper_parameter(__attribute__((unused)) struct Parser *parser, va_list *args) {
    uint32_t param = va_arg(*args, uint32_t);
    f_log_uhex_upper(param, parser->width, &parser->paddingCharacter, parser->useLeftAlignment);
}

static void Parser_unsigned_long_long_hex_upper_parameter(__attribute__((unused)) struct Parser *parser, va_list *args) {
    uint64_t param = va_arg(*args, uint64_t);
    f_log_uhex_upper(param, parser->width, &parser->paddingCharacter, parser->useLeftAlignment);
}

static void Parser_parameter(struct Parser *parser, __attribute__((unused)) va_list *args) {
    parser->parameterBuffer[parser->parameterCharCount] = parser->c;
    ++parser->parameterCharCount;
}

static void Parser_invalid_parameter(struct Parser *parser, __attribute__((unused)) va_list *args) {
    g_callback(parser->parameterBuffer, parser->parameterCharCount, false);
    g_callback(&parser->c, 1, false);
}

static void Parser_width_parameter(struct Parser *parser, __attribute__((unused)) va_list *args) {
    parser->parameterBuffer[parser->parameterCharCount] = parser->c;
    ++parser->parameterCharCount;
    unsigned digit = parser->c - '0';
    parser->width = parser->width * 10U + digit;
}

static void Parser_zero_padding_character(struct Parser *parser, __attribute__ ((unused)) va_list *args) {
    parser->parameterBuffer[parser->parameterCharCount] = parser->c;
    ++parser->parameterCharCount;
    parser->paddingCharacter = '0';
    parser->width = 0;
}

static void Parser_left_padding(struct Parser *parser, __attribute__((unused)) va_list *args) {
    parser->parameterBuffer[parser->parameterCharCount] = parser->c;
    ++parser->parameterCharCount;
    parser->useLeftAlignment = true;
}

struct Transition const TRANSITIONS[] = {
    STATE_NORMAL,    STATE_NEW,       '%', Parser_new,
    STATE_NORMAL,    STATE_NORMAL,     -1, Parser_normal,
    STATE_NEW,       STATE_NEW,       '0', Parser_zero_padding_character,
    STATE_NEW,       STATE_NEW,       '-', Parser_left_padding,
    STATE_NEW,       STATE_WIDTH_1,   '1', Parser_width_parameter,
    STATE_NEW,       STATE_WIDTH_1,   '2', Parser_width_parameter,
    STATE_NEW,       STATE_WIDTH_1,   '3', Parser_width_parameter,
    STATE_NEW,       STATE_WIDTH_1,   '4', Parser_width_parameter,
    STATE_NEW,       STATE_WIDTH_1,   '5', Parser_width_parameter,
    STATE_NEW,       STATE_WIDTH_1,   '6', Parser_width_parameter,
    STATE_NEW,       STATE_WIDTH_1,   '7', Parser_width_parameter,
    STATE_NEW,       STATE_WIDTH_1,   '8', Parser_width_parameter,
    STATE_NEW,       STATE_WIDTH_1,   '9', Parser_width_parameter,
    STATE_NEW,       STATE_NORMAL,    '%', Parser_normal,
    STATE_NEW,       STATE_NORMAL,    's', Parser_string_parameter,
    STATE_NEW,       STATE_NORMAL,    'u', Parser_unsigned_decimal_parameter,
    STATE_NEW,       STATE_NORMAL,    'd', Parser_signed_decimal_parameter,
    STATE_NEW,       STATE_NORMAL,    'x', Parser_hex_lower_parameter,
    STATE_NEW,       STATE_NORMAL,    'X', Parser_hex_upper_parameter,
    STATE_NEW,       STATE_LONG,      'l', Parser_parameter,
    STATE_NEW,       STATE_NORMAL,     -1, Parser_invalid_parameter,
    STATE_LONG,      STATE_NORMAL,    'u', Parser_unsigned_long_decimal_parameter,
    STATE_LONG,      STATE_NORMAL,    'd', Parser_signed_long_decimal_parameter,
    STATE_LONG,      STATE_NORMAL,    'x', Parser_unsigned_long_hex_lower_parameter,
    STATE_LONG,      STATE_NORMAL,    'X', Parser_unsigned_long_hex_upper_parameter,
    STATE_LONG,      STATE_LONG_LONG, 'l', Parser_parameter,
    STATE_LONG,      STATE_NORMAL,     -1, Parser_invalid_parameter,
    STATE_LONG_LONG, STATE_NORMAL,    'u', Parser_unsigned_long_long_decimal_parameter,
    STATE_LONG_LONG, STATE_NORMAL,    'd', Parser_signed_long_long_decimal_parameter,
    STATE_LONG_LONG, STATE_NORMAL,    'x', Parser_unsigned_long_long_hex_lower_parameter,
    STATE_LONG_LONG, STATE_NORMAL,    'X', Parser_unsigned_long_long_hex_upper_parameter,
    STATE_LONG_LONG, STATE_NORMAL,     -1, Parser_invalid_parameter,
    STATE_WIDTH_1,   STATE_WIDTH_2,   '0', Parser_width_parameter,
    STATE_WIDTH_1,   STATE_WIDTH_2,   '1', Parser_width_parameter,
    STATE_WIDTH_1,   STATE_WIDTH_2,   '2', Parser_width_parameter,
    STATE_WIDTH_1,   STATE_WIDTH_2,   '3', Parser_width_parameter,
    STATE_WIDTH_1,   STATE_WIDTH_2,   '4', Parser_width_parameter,
    STATE_WIDTH_1,   STATE_WIDTH_2,   '5', Parser_width_parameter,
    STATE_WIDTH_1,   STATE_WIDTH_2,   '6', Parser_width_parameter,
    STATE_WIDTH_1,   STATE_WIDTH_2,   '7', Parser_width_parameter,
    STATE_WIDTH_1,   STATE_WIDTH_2,   '8', Parser_width_parameter,
    STATE_WIDTH_1,   STATE_WIDTH_2,   '9', Parser_width_parameter,
    STATE_WIDTH_1,   STATE_NORMAL,    's', Parser_string_parameter,
    STATE_WIDTH_1,   STATE_NORMAL,    'u', Parser_unsigned_decimal_parameter,
    STATE_WIDTH_1,   STATE_NORMAL,    'd', Parser_signed_decimal_parameter,
    STATE_WIDTH_1,   STATE_NORMAL,    'x', Parser_hex_lower_parameter,
    STATE_WIDTH_1,   STATE_NORMAL,    'X', Parser_hex_upper_parameter,
    STATE_WIDTH_1,   STATE_LONG,      'l', Parser_parameter,
    STATE_WIDTH_1,   STATE_NORMAL,     -1, Parser_invalid_parameter,
    STATE_WIDTH_2,   STATE_NORMAL,    's', Parser_string_parameter,
    STATE_WIDTH_2,   STATE_NORMAL,    'u', Parser_unsigned_decimal_parameter,
    STATE_WIDTH_2,   STATE_NORMAL,    'd', Parser_signed_decimal_parameter,
    STATE_WIDTH_2,   STATE_NORMAL,    'x', Parser_hex_lower_parameter,
    STATE_WIDTH_2,   STATE_NORMAL,    'X', Parser_hex_upper_parameter,
    STATE_WIDTH_2,   STATE_LONG,      'l', Parser_parameter,
    STATE_WIDTH_2,   STATE_NORMAL,     -1, Parser_invalid_parameter
};
const int TRANSITION_COUNT = sizeof(TRANSITIONS) / sizeof(struct Transition);

static void Parser_next_char(struct Parser *parser, char c, va_list *args) {
    parser->c = c;
    for (int i = 0; i < TRANSITION_COUNT; ++i) {
        struct Transition const *transition = TRANSITIONS + i;
        if (parser->state == transition->begin_state && transition->c == -1) {
            transition->action(parser, args);
            parser->state = transition->end_state;
            return;
        }
        if (parser->state == transition->begin_state && c == transition->c) {
            transition->action(parser, args);
            parser->state = transition->end_state;
            return;
        }
    }
    g_callback("FATAL ", 5, false);
    g_callback(__FILE__, strlen(__FILE__), false);
    g_callback(" (", 1, false);
    f_log_udec(__LINE__, 0, " ", false);
    const char * const MESSAGE = "):  Logger in invalid state";
    g_callback(MESSAGE, strlen(MESSAGE), true);
    f_abort();
}

static void f_log_message(const char *message, va_list args) {
    const char *pos = message;
    struct Parser parser;
    Parser_init(&parser);
    while (*pos != 0) {
        Parser_next_char(&parser, *pos, &args);
        ++pos;
    }
    if (parser.state != STATE_NORMAL) {
        g_callback(parser.parameterBuffer, parser.parameterCharCount, false);
    }
}

void f_log(int logLevel, const char *message, const char *fileName, size_t lineNumber, ...) {
    f_log_level(logLevel);
    g_callback(fileName, strlen(fileName), false);
    g_callback(" (", 2, false);
    f_log_udec(lineNumber, 0, " ", false);
    g_callback("):  ", 4, false);
    va_list args;
    va_start(args, lineNumber);
    f_log_message(message, args);
    g_callback("", 0, true);
    va_end(args);
}

void f_log_callback(FLogCallback callback) {
    g_callback = callback;
}
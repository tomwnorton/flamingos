#ifndef __FLAMINGOS_FERROR_H__
#define __FLAMINGOS_FERROR_H__

enum FError {
    FERROR_OK,
    FERROR_OUT_OF_MEMORY,
    FERROR_NULL_REFERENCE,
    FERROR_DUPLICATE_KEY,
    FERROR_ELEMENT_NOT_FOUND
};

#endif
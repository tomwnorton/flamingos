#ifndef FLAMINGOS_PIC_IO_H
#define FLAMINGOS_PIC_IO_H

EXTERN_C uint8_t f_outb(uint16_t portNumber, uint8_t byte);
EXTERN_C uint8_t f_inb(uint16_t portNumber);

#endif // FLAMINGOS_PIC_IO_H
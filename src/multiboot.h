#ifndef FLAMINGOS_MULTIBOOT_H
#define FLAMINGOS_MULTIBOOT_H

#define F_MULTIBOOT_FLAG_MEMORY (1UL << 0UL)
#define F_MULTIBOOT_FLAG_BOOT_DEVICE (1UL << 1UL)
#define F_MULTIBOOT_FLAG_CMDLINE (1UL << 2UL)
#define F_MULTIBOOT_FLAG_MODULES (1UL << 3UL)
#define F_MULTIBOOT_FLAG_SYMBOLS_A_OUT (1UL << 4UL)
#define F_MULTIBOOT_FLAG_SYMBOLS_ELF (1UL << 5UL)
#define F_MULTIBOOT_FLAG_MEMORY_MAP (1UL << 6UL)
#define F_MULTIBOOT_FLAG_DRIVES (1UL << 7UL)
#define F_MULTIBOOT_FLAG_CONFIG_TABLE (1UL << 8UL)
#define F_MULTIBOOT_FLAG_BOOT_LOADER_NAME (1UL << 9UL)
#define F_MULTIBOOT_FLAG_APM_TABLE (1UL << 10UL)
#define F_MULTIBOOT_FLAG_VESA_BIOS_EXTENSIONS (1UL << 11UL)

struct MultibootSymbolsAOut {
    unsigned int tab_size;
    unsigned int string_size;
    void *address;
    unsigned int reserved;
};

struct MultibootSymbolsElf {
    unsigned int num;
    unsigned int size;
    void *address;
    unsigned int shndx;
};

union MultibootSymbols {
    struct MultibootSymbolsAOut a_out;
    struct MultibootSymbolsElf elf;
};

struct MultibootMemoryMapEntry {
    unsigned int size;
    unsigned int base_addr_low;
    unsigned int base_addr_high;
    unsigned int length_low;
    unsigned int length_high;
    unsigned int type; /// 1 indicates usable RAM
};

struct MultibootDriveEntry {
    unsigned int size;
    unsigned char drive_number;
    unsigned char drive_mode;
    unsigned short drive_cylinders;
    unsigned char drive_heads;
    unsigned char drive_sectors_per_track;
    unsigned int drive_ports[];
};

struct MultibootAdvancedPowerManagementTable {
    unsigned short version;
    unsigned short segment;
    unsigned int offset;
    unsigned short cseg_16;
    unsigned short dseg_16;
    unsigned short flags;
    unsigned short segment_len;
    unsigned short cseg_16_len;
    unsigned short dseg_16_len;
};

struct MultibootInfo {
    unsigned int flags;
    unsigned int memory_lower;
    unsigned int memory_upper;
    unsigned int boot_device;
    const char *command_line;
    unsigned int modules_count;
    void *modules_address;
    union MultibootSymbols symbols;
    unsigned int memory_map_length;
    struct MultibootMemoryMapEntry *memory_map_entries;
    unsigned int drives_length;
    struct MultibootDriveEntry *drive_entries;
    void *config_table;
    const char *boot_loader_name;
    struct MultibootAdvancedPowerManagementTable *apm_table;
    void *vbe_control_info; // Address of control info structure returned by VBE 00h
    void *vbe_mode_info;    // Address of mode info structure returned by VBE 01h;
    unsigned short vbe_mode; // Current video mode, per VBE 3.0 spec
    // Next 3 are the addresses of the protected mode interface defined by VBE 2.0+
    unsigned short vbe_interface_segment;
    unsigned short vbe_interface_offset;
    unsigned short vbe_interface_length;
};

#endif // FLAMINGOS_MULTIBOOT_H
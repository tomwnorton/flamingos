#include "string.h"

size_t strlen(const char *s) {
    size_t count = 0;
    for (; *s != 0; ++count, ++s);
    return count;
}

int strncmp(const char *lhs, const char *rhs, unsigned int count) {
    for (size_t i = 0UL; i < count; ++i) {
        if (lhs[i] < rhs[i]) return -1;
        if (lhs[i] > rhs[i]) return 1;
    }
    return 0;
}
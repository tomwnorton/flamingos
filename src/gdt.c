#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include "gdt.h"

#define DIM(x) (sizeof(x) / sizeof(x[0]))
#define PAGE_SIZE 0x1000
#define PAGE_SIZE_BIT_SHIFT 12
#define BYTES_TO_PAGES(bytesCount) ((bytesCount) >> PAGE_SIZE_BIT_SHIFT)


struct GDTAccessByte GDT_ACCESS_BYTES[] = {
        {0, 0, 0, 0, 0, 0, 0},
        {0, 1, 0, 1, 1, 0, 1},
        {0, 0, 0, 1, 1, 0, 1},
        {0, 1, 0, 0, 1, 0, 1},
        {0, 0, 0, 0, 1, 0, 1},
        {1, 0, 0, 1, 0, 0, 1}
};

struct GDTFlags GDT_FLAGS[] = {
        { false, false, true, false, 0 },
        { false, false, true, true,  0 }
};

/**
 * \brief This is the Task State Segment
 *
 * This is used for hardware multitasking, and we should have at least one of these segments in our GDT.
 * Perhaps we will need one for each processor?
 */
unsigned long long TSS[13UL];

struct GDTEntryModel FLAT_MODEL[] = {
        {0x00,     0x00,                       GDT_SEGMENT_TYPE_NULL,              0x00},
        {0x00,     BYTES_TO_PAGES(0xFFFFFFFF), GDT_SEGMENT_TYPE_CODE_EXECUTE_ONLY, GDT_GRANULARITY_PAGES},
        {0x00,     BYTES_TO_PAGES(0xFFFFFFFF), GDT_SEGMENT_TYPE_DATA_READ_WRITE,   GDT_GRANULARITY_PAGES},
        {0,        sizeof(TSS),                GDT_SEGMENT_TYPE_TASK_STATE,        GDT_GRANULARITY_BYTES}
};
size_t const FLAT_MODEL_COUNT = DIM(FLAT_MODEL);

bool f_populate_gdt_entries(struct GDTEntryModel *models, struct GDTEntryModel *modelsEnd, struct GDTEntry *entries) {
    struct GDTEntry *entry = entries;
    for (struct GDTEntryModel *model = models; model != modelsEnd; ++model, ++entry) {
        if (model->limit >= 0x100000) return false;
        if (model->type >= DIM(GDT_ACCESS_BYTES)) return false;
        if (model->granularity >= DIM(GDT_FLAGS)) return false;
        entry->limit_lower = (model->limit & 0xFFFF);
        entry->limit_upper = model->limit >> 16;
        entry->base_lower = (model->base & 0xFFFFFF);
        entry->base_upper = model->base >> 24;
        entry->access_byte = *((char *)(GDT_ACCESS_BYTES + model->type));
        if (model->type == GDT_SEGMENT_TYPE_NULL) {
            entry->flags = 0;
        } else {
            entry->flags = *((char *) (GDT_FLAGS + model->granularity));
        }
    }
    return true;
}

void f_initialize_gdt_model(void) {
    FLAT_MODEL[FLAT_MODEL_COUNT - 1].base = (size_t) &TSS;
}

bool f_setup_gdt(void) {
    struct GDTEntry entries[FLAT_MODEL_COUNT];
    f_initialize_gdt_model();
    if (!f_populate_gdt_entries(FLAT_MODEL, FLAT_MODEL + FLAT_MODEL_COUNT, entries)) {
        return false;
    }
    f_load_gdt(entries, sizeof(entries));
    f_reload_segments();
    return true;
}
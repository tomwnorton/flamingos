//
// Created by thomas on 2/1/22.
//

#ifndef FLAMINGOS_MULTIBOOT_MEMORY_MAP_ITERATOR_H
#define FLAMINGOS_MULTIBOOT_MEMORY_MAP_ITERATOR_H

struct MultibootMemoryMapIterator {
    size_t length;
    size_t address;
};

EXTERN_C enum FError mmap_iterator_has_next(struct MultibootMemoryMapIterator *, bool *result);
EXTERN_C enum FError mmap_iterator_move_next(struct MultibootMemoryMapIterator *);
EXTERN_C enum FError mmap_iterator_get_start_address(struct MultibootMemoryMapIterator *, unsigned long long *result);
EXTERN_C enum FError mmap_iterator_get_length(struct MultibootMemoryMapIterator *, unsigned long long *result);
EXTERN_C enum FError mmap_iterator_is_usable(struct MultibootMemoryMapIterator *, bool *result);

#endif //FLAMINGOS_MULTIBOOT_MEMORY_MAP_H

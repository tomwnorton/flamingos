#include <stddef.h>
#include <stdbool.h>
#include "cxx.h"
#include "kerror.h"
#include "memory/red-black-tree.h"
#include "memory/red-black-tree-internal.h"

#define NODE_COLOR(node) (node == NULL ? KMRBT_BLACK : node->color)
#define COLOR_NODE_BLACK(node) if (node != NULL) node->color = KMRBT_BLACK
#define COLOR_NODE_RED(node) node->color = KMRBT_RED

struct MemoryRedBlackTreeNode *f_mrbtn_get_minimum(struct MemoryRedBlackTreeNode *node) {
    while (node->left != NULL) {
        node = node->left;
    }
    return node;
}

struct MemoryRedBlackTreeNode *f_mrbtn_get_maximum(struct MemoryRedBlackTreeNode *node) {
    while (node->right != NULL) {
        node = node->right;
    }
    return node;
}

struct MemoryRedBlackTreeNode *f_mrbtn_get_successor(struct MemoryRedBlackTreeNode *node) {
    if (node->right != NULL) {
        return f_mrbtn_get_minimum(node->right);
    }
    while (node != NULL) {
        if (node->parent != NULL && node == node->parent->left) {
            return node->parent;
        }
        node = node->parent;
    }
    return NULL;
}

struct MemoryRedBlackTreeNode *f_mrbtn_get_predecessor(struct MemoryRedBlackTreeNode *node) {
    if (node->left != NULL) {
        return f_mrbtn_get_maximum(node->left);
    }
    while (node != NULL) {
        if (node->parent != NULL && node == node->parent->right) {
            return node->parent;
        }
        node = node->parent;
    }
    return NULL;
}

enum FError f_mrbt_init(struct MemoryRedBlackTree *self) {
    if (self == NULL) return FERROR_NULL_REFERENCE;
    self->head_block = NULL;
    self->tail_block = NULL;
    self->root = NULL;
    return FERROR_OK;
}

enum FError f_mrbt_add_allocation(struct MemoryRedBlackTree *self, struct MemoryRedBlackTreeNodeBlock *block) {
    if (self == NULL || block == NULL) return FERROR_NULL_REFERENCE;
    block->next = NULL;
    struct MemoryRedBlackTreeNode *end = block->nodes + block->length;
    for (struct MemoryRedBlackTreeNode *node = block->nodes; node != end; ++node) {
        node->color = KMRBT_UNUSED;
    }
    if (self->head_block == NULL) {
        self->head_block = block;
        self->tail_block = block;
    } else {
        self->tail_block->next = block;
        self->tail_block = block;
    }
    return FERROR_OK;
}

inline static struct MemoryRedBlackTreeNode *find_unused_node_in_block(struct MemoryRedBlackTreeNodeBlock *block) {
    struct MemoryRedBlackTreeNode *end = block->nodes + block->length;
    for (struct MemoryRedBlackTreeNode *node = block->nodes; node != end; ++node) {
        if (node->color == KMRBT_UNUSED) {
            return node;
        }
    }
    return NULL;
}

enum FError f_mrbt_create_node(struct MemoryRedBlackTree *self, struct MemoryRedBlackTreeNode **result, size_t offset, size_t length) {
    struct MemoryRedBlackTreeNodeBlock *block = self->head_block;
    while (block != NULL) {
        struct MemoryRedBlackTreeNode *node = find_unused_node_in_block(block);
        if (node != NULL) {
            node->color = KMRBT_RED;
            node->offset = offset;
            node->length = length;
            node->left = NULL;
            node->right = NULL;
            *result = node;
            return FERROR_OK;
        }
        block = block->next;
    }
    return FERROR_OUT_OF_MEMORY;
}

enum FError f_mrbt_insert_node(struct MemoryRedBlackTree *self, struct MemoryRedBlackTreeNode *newNode) {
    if (self->root == NULL) {
        self->root = newNode;
        self->root->parent = NULL;
    } else {
        struct MemoryRedBlackTreeNode *current = self->root;
        while (current != NULL) {
            if (newNode->offset < current->offset) {
                if (current->left == NULL) {
                    current->left = newNode;
                    newNode->parent = current;
                    break;
                }
                current = current->left;
            } else if (newNode->offset > current->offset) {
                if (current->right == NULL) {
                    current->right = newNode;
                    newNode->parent = current;
                    break;
                }
                current = current->right;
            } else {
                return FERROR_DUPLICATE_KEY;
            }
        }
    }
    return FERROR_OK;
}

static void rotate_node_left(struct MemoryRedBlackTreeNode* node) {
    struct MemoryRedBlackTreeNode* parent = node->parent;
    struct MemoryRedBlackTreeNode* right = node->right;

    right->parent = node->parent;
    node->right = right->left;
    node->parent = right;
    right->left = node;
    if (parent != NULL) {
        if (node == parent->left) {
            parent->left = right;
        } else {
            parent->right = right;
        }
    }
}

static void rotate_node_right(struct MemoryRedBlackTreeNode* node) {
    struct MemoryRedBlackTreeNode* parent = node->parent;
    struct MemoryRedBlackTreeNode* left = node->left;

    left->parent = node->parent;
    node->left = left->right;
    node->parent = left;
    left->right = node;
    if (parent != NULL) {
        if (node == parent->left) {
            parent->left = left;
        } else {
            parent->right = left;
        }
    }
}

void f_mrbt_insert_fixup(struct MemoryRedBlackTree *self, struct MemoryRedBlackTreeNode *newNode) {
    while (NODE_COLOR(newNode->parent) == KMRBT_RED) {
        struct MemoryRedBlackTreeNode *parent = newNode->parent;
        struct MemoryRedBlackTreeNode *grandparent = parent->parent;
        struct MemoryRedBlackTreeNode *uncle;
        if (parent == grandparent->left) {
            uncle = grandparent->right;
        } else {
            uncle = grandparent->left;
        }
        if (NODE_COLOR(uncle) == KMRBT_BLACK) {
            if (newNode == parent->right && parent == grandparent->left) {
                rotate_node_left(parent);
                newNode = parent;
                continue;
            } else if (newNode == parent->left && parent == grandparent->right) {
                rotate_node_right(parent);
                newNode = parent;
                continue;
            }
            grandparent->color = KMRBT_RED;
            parent->color = KMRBT_BLACK;
            if (parent == grandparent->left) {
                rotate_node_right(grandparent);
            } else if (parent == grandparent->right) {
                rotate_node_left(grandparent);
            } else {
                parent->left = grandparent;
                grandparent->right = NULL;
            }
            if (self->root == grandparent) {
                self->root = parent;
            }
        } else {
            parent->color = uncle->color = KMRBT_BLACK;
            grandparent->color = KMRBT_RED;
            newNode = grandparent;
            continue;
        }
    }
    self->root->color = KMRBT_BLACK;
}

enum FError f_mrbt_insert_element(struct MemoryRedBlackTree *self, size_t offset, size_t length) {
    if (self == NULL) return FERROR_NULL_REFERENCE;
    struct MemoryRedBlackTreeNode* node;
    enum FError error = f_mrbt_create_node(self, &node, offset, length);
    if (error != FERROR_OK) return error;
    error = f_mrbt_insert_node(self, node);
    if (error != FERROR_OK) return error;
    f_mrbt_insert_fixup(self, node);
    return FERROR_OK;
}

enum FError f_mrbt_find_element(struct MemoryRedBlackTree *self, size_t address, struct MemoryRedBlackTreeIterator *iterator) {
    if (self == NULL || iterator == NULL) return FERROR_NULL_REFERENCE;
    struct MemoryRedBlackTreeNode *node = self->root;
    while (node != NULL) {
        if (address < node->offset) {
            node = node->left;
        } else if (address > node->offset + node->length) {
            node = node->right;
        } else {
            iterator->current = node;
            iterator->forward = true;
            return FERROR_OK;
        }
    }
    return FERROR_ELEMENT_NOT_FOUND;
}

void f_mrbt_transplant(struct MemoryRedBlackTree *self, struct MemoryRedBlackTreeNode *pos, struct MemoryRedBlackTreeNode *node) {
    if (pos->parent == NULL) {
        self->root = node;
        if (node != NULL) {
            node->parent = NULL;
        }
    } else if (pos == pos->parent->left) {
        pos->parent->left = node;
        if (node != NULL) {
            node->parent = pos->parent;
        }
    } else if (pos == pos->parent->right) {
        pos->parent->right = node;
        if (node != NULL) {
            node->parent = pos->parent;
        }
    }
}

void f_mrbt_remove_node(struct MemoryRedBlackTree *self, struct MemoryRedBlackTreeNode *z, struct MemoryRedBlackTreeNode **x, struct MemoryRedBlackTreeNode **xParent, enum MemoryRedBlackTreeColor *yOriginalColor) {
    struct MemoryRedBlackTreeNode *y = z;
    *yOriginalColor = y->color;
    *xParent = z->parent;
    if (z->right == NULL) {
        *x = y->left;
        f_mrbt_transplant(self, z, z->left);
        z->color = KMRBT_UNUSED;
    } else if (z->left == NULL) {
        *x = y->right;
        f_mrbt_transplant(self, z, z->right);
        z->color = KMRBT_UNUSED;
    } else {
        y = f_mrbtn_get_minimum(z->right);
        *yOriginalColor = y->color;
        *x = y->right;
        if (y->parent == z) {
            if (*x != NULL) {
                (*x)->parent = *xParent = y;
            }
        } else {
            f_mrbt_transplant(self, y, *x);
            *xParent = (*x)->parent;
            y->right = z->right;
            if (y->right != NULL) {
                y->right->parent = y;
            }
        }
        f_mrbt_transplant(self, z, y);
        y->left = z->left;
        if (y->left != NULL) {
            y->left->parent = y;
        }
        y->color = z->color;
        z->color = KMRBT_UNUSED;
    }
}

/*
 * Some gotchas in case I need to come back to this:
 * - As far as I can tell, so long as the red/black tree is valid, w can never be null.  These needs restating:
 *
 *               W. CAN. NEVER. BE. NULL!!!!!!!!!!!!!
 *   All of my issues with implementing a free list with a red/black tree have been because I've assumed w can be null.
 *   I know I am going to forget this, so I am putting this note here.
 */
void f_mrbt_remove_fixup(struct MemoryRedBlackTree *self, struct MemoryRedBlackTreeNode *x, struct MemoryRedBlackTreeNode *xParent) {
    while (NODE_COLOR(x) == KMRBT_BLACK && x != self->root) {
        if (xParent != NULL) {
            if (x == xParent->left) {
                struct MemoryRedBlackTreeNode *w = xParent->right;
                if (NODE_COLOR(w) == KMRBT_RED) {
                    COLOR_NODE_BLACK(w);
                    COLOR_NODE_RED(xParent);
                    rotate_node_left(xParent);
                    w = xParent->right;
                }
                if (NODE_COLOR(w->left) == KMRBT_BLACK && NODE_COLOR(w->right) == KMRBT_BLACK) {
                    COLOR_NODE_RED(w);
                    x = xParent;
                    xParent = x->parent;
                } else {
                    if (NODE_COLOR(w->right) == KMRBT_BLACK) {
                        COLOR_NODE_BLACK(w->left);
                        COLOR_NODE_RED(w);
                        rotate_node_right(w);
                        w = xParent->right;
                    }
                    w->color = xParent->color;
                    xParent->color = KMRBT_BLACK;
                    // Handle a situation where both of w's children are red
                    // Without this, we can end up with a tree with more black
                    // nodes on the right side than on the left.
                    if (NODE_COLOR(w->left) == KMRBT_BLACK) {
                        w->right->color = KMRBT_BLACK;
                    }
                    rotate_node_left(xParent);
                    x = self->root;
                    xParent = NULL;
                }
            } else {
                struct MemoryRedBlackTreeNode *w = xParent->left;
                if (NODE_COLOR(w) == KMRBT_RED) {
                    COLOR_NODE_BLACK(w);
                    COLOR_NODE_RED(xParent);
                    rotate_node_right(xParent);
                    w = xParent->left;
                }
                if (NODE_COLOR(w->left) == KMRBT_BLACK && NODE_COLOR(w->right) == KMRBT_BLACK) {
                    COLOR_NODE_RED(w);
                    x = xParent;
                    xParent = x->parent;
                } else {
                    if (NODE_COLOR(w->left) == KMRBT_BLACK) {
                        COLOR_NODE_BLACK(w->right);
                        COLOR_NODE_RED(w);
                        rotate_node_left(w);
                        w = xParent->left;
                    }
                    w->color = xParent->color;
                    xParent->color = KMRBT_BLACK;
                    // Handle a situation where both of w's children are red
                    // Without this, we can end up with a tree with more black
                    // nodes on the right side than on the left.
                    if (NODE_COLOR(w->right) == KMRBT_BLACK) {
                        w->left->color = KMRBT_BLACK;
                    }
                    rotate_node_right(xParent);
                    x = self->root;
                    xParent = NULL;
                }
            }
        }
    }
    COLOR_NODE_BLACK(x);
}

inline static struct MemoryRedBlackTreeNode *find_root(struct MemoryRedBlackTreeNode *node) {
    while (node->parent != NULL) {
        node = node->parent;
    }
    return node;
}

enum FError f_mrbt_remove_element(struct MemoryRedBlackTree *self, struct MemoryRedBlackTreeIterator *iterator) {
    if (self == NULL || iterator == NULL || iterator->current == NULL) return FERROR_NULL_REFERENCE;
    if (find_root(iterator->current) != self->root) return FERROR_ELEMENT_NOT_FOUND;
    struct MemoryRedBlackTreeNode *x;
    struct MemoryRedBlackTreeNode *xParent;
    enum MemoryRedBlackTreeColor yOriginalColor;

    f_mrbt_remove_node(self, iterator->current, &x, &xParent, &yOriginalColor);
    if (yOriginalColor == KMRBT_BLACK) {
        f_mrbt_remove_fixup(self, x, xParent);
    }
    return FERROR_OK;
}

enum FError f_mrbt_get_iterator(struct MemoryRedBlackTree *self, struct MemoryRedBlackTreeIterator *result) {
    if (self == NULL || result == NULL) return FERROR_NULL_REFERENCE;
    result->current = self->root != NULL ? f_mrbtn_get_minimum(self->root) : NULL;
    result->forward = true;
    return FERROR_OK;
}

enum FError f_mrbti_has_elements(struct MemoryRedBlackTreeIterator *self, bool *result) {
    if (self == NULL || result == NULL) return FERROR_NULL_REFERENCE;
    *result = self->current != NULL;
    return FERROR_OK;
}

enum FError f_mrbti_move_next(struct MemoryRedBlackTreeIterator *self) {
    if (self == NULL) return FERROR_NULL_REFERENCE;
    if (self->current == NULL) return FERROR_ELEMENT_NOT_FOUND;
    self->current = self->forward ? f_mrbtn_get_successor(self->current) : f_mrbtn_get_predecessor(self->current);
    return FERROR_OK;
}

enum FError f_mrbti_clone(struct MemoryRedBlackTreeIterator *self, struct MemoryRedBlackTreeIterator *result) {
    if (self == NULL || result == NULL) return FERROR_NULL_REFERENCE;
    result->current = self->current;
    result->forward = self->forward;
    return FERROR_OK;
}

enum FError f_mrbti_clone_reversed(struct MemoryRedBlackTreeIterator *self, struct MemoryRedBlackTreeIterator *result) {
    if (self == NULL || result == NULL) return FERROR_NULL_REFERENCE;
    result->current = self->current;
    result->forward = !self->forward;
    return FERROR_OK;
}

enum FError f_mrbti_get_offset(struct MemoryRedBlackTreeIterator *self, size_t *result) {
    if (self == NULL || result == NULL) return FERROR_NULL_REFERENCE;
    if (self->current == NULL) return FERROR_ELEMENT_NOT_FOUND;
    *result = self->current->offset;
    return FERROR_OK;
}

enum FError f_mrbti_get_length(struct MemoryRedBlackTreeIterator *self, size_t *result) {
    if (self == NULL || result == NULL) return FERROR_NULL_REFERENCE;
    if (self->current == NULL) return FERROR_ELEMENT_NOT_FOUND;
    *result = self->current->length;
    return FERROR_OK;
}

enum FError f_mrbti_get_color(struct MemoryRedBlackTreeIterator *self, enum MemoryRedBlackTreeColor *result) {
    if (self == NULL || result == NULL) return FERROR_NULL_REFERENCE;
    if (self->current == NULL) return FERROR_ELEMENT_NOT_FOUND;
    *result = self->current->color;
    return FERROR_OK;
}

enum FError f_mrbti_set_offset(struct MemoryRedBlackTreeIterator *self, size_t offset) {
    if (self == NULL) return FERROR_NULL_REFERENCE;
    if (self->current == NULL) return FERROR_ELEMENT_NOT_FOUND;
    self->current->offset = offset;
    return FERROR_OK;
}

enum FError f_mrbti_set_length(struct MemoryRedBlackTreeIterator *self, size_t length) {
    if (self == NULL) return FERROR_NULL_REFERENCE;
    if (self->current == NULL) return FERROR_ELEMENT_NOT_FOUND;
    self->current->length = length;
    return FERROR_OK;
}
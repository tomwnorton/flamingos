#include <stddef.h>
#include <stdbool.h>
#include "cxx.h"
#include "kerror.h"
#include "multiboot.h"
#include "memory/multiboot-memory-map-iterator.h"

enum FError mmap_iterator_has_next(struct MultibootMemoryMapIterator *this, bool *result) {
    if (this == NULL || result == NULL) return FERROR_NULL_REFERENCE;
    *result = this->length > 0UL;
    return FERROR_OK;
}

enum FError mmap_iterator_move_next(struct MultibootMemoryMapIterator *this) {
    if (this == NULL) return FERROR_NULL_REFERENCE;
    if (this->length == 0UL) return FERROR_ELEMENT_NOT_FOUND;
    struct MultibootMemoryMapEntry *entry = (struct MultibootMemoryMapEntry *) this->address;
    unsigned int skipSize = entry->size + sizeof(unsigned int);
    this->length -= skipSize;
    this->address += skipSize;
    return FERROR_OK;
}

enum FError mmap_iterator_get_start_address(struct MultibootMemoryMapIterator *this, unsigned long long *result) {
    if (this == NULL || result == NULL) return FERROR_NULL_REFERENCE;
    if (this->length == 0UL) return FERROR_ELEMENT_NOT_FOUND;
    struct MultibootMemoryMapEntry *entry = (struct MultibootMemoryMapEntry *) this->address;
    *result = ((((unsigned long long) entry->base_addr_high) << 32ULL) | ((unsigned long long) entry->base_addr_low));
    return FERROR_OK;
}

enum FError mmap_iterator_get_length(struct MultibootMemoryMapIterator *this, unsigned long long *result) {
    if (this == NULL || result == NULL) return FERROR_NULL_REFERENCE;
    if (this->length == 0UL) return FERROR_ELEMENT_NOT_FOUND;
    struct MultibootMemoryMapEntry *entry = (struct MultibootMemoryMapEntry *) this->address;
    *result = ((((unsigned long long) entry->length_high) << 32ULL) | ((unsigned long long) entry->length_low));
    return FERROR_OK;
}

enum FError mmap_iterator_is_usable(struct MultibootMemoryMapIterator *this, bool *result) {
    if (this == NULL || result == NULL) return FERROR_NULL_REFERENCE;
    if (this->length == 0UL) return FERROR_ELEMENT_NOT_FOUND;
    struct MultibootMemoryMapEntry *entry = (struct MultibootMemoryMapEntry *) this->address;
    *result = entry->type == 1UL;
    return FERROR_OK;
}
#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#define F_PAGE_DIRECTORY_PRESENT               0x0001UL
#define F_PAGE_DIRECTORY_WRITABLE              0x0002UL
#define F_PAGE_DIRECTORY_USER_ACCESSIBLE       0x0004UL
#define F_PAGE_DIRECTORY_PAGE_WRITE_THROUGH    0X0008UL
#define F_PAGE_DIRECTORY_PAGE_CACHE_DISABLED   0x0010UL
#define F_PAGE_DIRECTORY_ACCESSED              0x0020UL
#define F_PAGE_DIRECTORY_ADDRESS_OFFSET        12UL
#define F_PAGE_DIRECTORY_ADDRESS_MASK          0xFFFFF000UL

#define F_PAGE_TABLE_PRESENT                    0x0001UL
#define F_PAGE_TABLE_WRITABLE                   0x0002UL
#define F_PAGE_TABLE_USER_ACCESSIBLE            0x0004UL
#define F_PAGE_TABLE_PAGE_WRITE_THROUGH         0X0008UL
#define F_PAGE_TABLE_PAGE_CACHE_DISABLED        0x0010UL
#define F_PAGE_TABLE_ACCESSED                   0x0020UL
#define F_PAGE_TABLE_DIRTY                      0x0040UL
#define F_PAGE_TABLE_PAGE_ATTRIBUTE_TABLE       0x0080UL
#define F_PAGE_TABLE_GLOBAL                     0X0100UL
#define F_PAGE_TABLE_ADDRESS_OFFSET             12UL
#define F_PAGE_TABLE_ADDRESS_MASK               0xFFFFF000UL

static inline size_t f_get_page_index(uint8_t *ptr) {
    return ((size_t) ptr) / 4096UL;
}

/**
 * Set up identity paging for the first 1MiB of memory.  This means
 * that physical address 0x00ABCD should translate to address 0x00ABCD.
 * 
 * This ensures that the BIOS memory map will still work.  It also ensures
 * that we can emulate DOS programs if we want to, since they have to
 * exist in the bottom 1MiB of memory.
 * 
 * This also performs an identity paging for a little higher than the 1MiB
 * range.  This is because the code that runs before paging, including the
 * code that initializes paging is a LOT easier to write if it exists in
 * identity pages.
 */
static void f_setup_identity_paging(size_t pageDirectory[1024],
                                    size_t pageTable[1024],
                                    uint8_t *kernelStart) {
    pageDirectory[0] = ((size_t) pageTable | F_PAGE_DIRECTORY_PRESENT | F_PAGE_DIRECTORY_WRITABLE);
    unsigned int i = 0;
    unsigned int maxIdentityPage = f_get_page_index(kernelStart);
    for (; i < maxIdentityPage; ++i) {
        pageTable[i] = ((i << F_PAGE_TABLE_ADDRESS_OFFSET) | F_PAGE_TABLE_PRESENT | F_PAGE_TABLE_WRITABLE);
    }
    for (; i < 1024; ++i) {
        pageTable[i] = 0UL;
    }
}

/**
 * \brief Most of the kernel should exist in the highest 1GiB of memory
 * 
 * This ensures that programs in userspace can exist in the bottom 3GiB
 * of memory and think that they have access to almost all memroy.
 */
static void f_setup_kernel_paging(size_t pageDirectory[1024],
                                  size_t pageTable[1024],
                                  uint8_t *kernelStart,
                                  uint8_t *writableDataStart) {
    unsigned int i = 0;
    unsigned int kernelOffset = f_get_page_index(kernelStart);
    unsigned int j = kernelOffset;
    unsigned int writableDataPage = f_get_page_index(writableDataStart);
    pageDirectory[768] = ((size_t) pageTable | F_PAGE_DIRECTORY_PRESENT | F_PAGE_DIRECTORY_WRITABLE);
    for (; i < writableDataPage - kernelOffset; ++i, ++j) {
        pageTable[i] = ((j << F_PAGE_TABLE_ADDRESS_OFFSET) | F_PAGE_TABLE_PRESENT);
    }
    for (; i < 1024UL; ++i, ++j) {
        pageTable[i] = ((j << F_PAGE_TABLE_ADDRESS_OFFSET | F_PAGE_TABLE_PRESENT | F_PAGE_TABLE_WRITABLE));
    }
}

void f_setup_paging(size_t pageDirectory[1024],
                    size_t firstPageTable[1024],
                    size_t secondPageTable[1024],
                    uint8_t *kernelStart,
                    uint8_t *writableDataStart) {
    f_setup_identity_paging(pageDirectory, firstPageTable, kernelStart);
    f_setup_kernel_paging(pageDirectory, secondPageTable, kernelStart, writableDataStart);
}
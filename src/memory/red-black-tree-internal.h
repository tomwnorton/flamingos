//
// Created by thomas on 12/16/21.
//

#ifndef FLAMINGOS_RED_BLACK_TREE_INTERNAL_H
#define FLAMINGOS_RED_BLACK_TREE_INTERNAL_H

EXTERN_C_START
enum FError f_mrbt_create_node(struct MemoryRedBlackTree *self, struct MemoryRedBlackTreeNode **result, size_t offset, size_t length);
enum FError f_mrbt_insert_node(struct MemoryRedBlackTree *self, struct MemoryRedBlackTreeNode *newNode);
void f_mrbt_insert_fixup(struct MemoryRedBlackTree *self, struct MemoryRedBlackTreeNode *newNode);

void f_mrbt_transplant(struct MemoryRedBlackTree *self, struct MemoryRedBlackTreeNode *pos, struct MemoryRedBlackTreeNode *node);
void f_mrbt_remove_node(struct MemoryRedBlackTree *self, struct MemoryRedBlackTreeNode *z, struct MemoryRedBlackTreeNode **x, struct MemoryRedBlackTreeNode **xParent, enum MemoryRedBlackTreeColor *yOriginalColor);
void f_mrbt_remove_fixup(struct MemoryRedBlackTree *self, struct MemoryRedBlackTreeNode *x, struct MemoryRedBlackTreeNode *xParent);

struct MemoryRedBlackTreeNode *f_mrbtn_get_minimum(struct MemoryRedBlackTreeNode *node);
struct MemoryRedBlackTreeNode *f_mrbtn_get_maximum(struct MemoryRedBlackTreeNode *node);
struct MemoryRedBlackTreeNode *f_mrbtn_get_successor(struct MemoryRedBlackTreeNode *node);
struct MemoryRedBlackTreeNode *f_mrbtn_get_predecessor(struct MemoryRedBlackTreeNode *node);
EXTERN_C_END

#endif //FLAMINGOS_RED_BLACK_TREE_INTERNAL_H

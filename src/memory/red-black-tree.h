#ifndef __FLAMINGS_MEMORY_RED_BLACK_TREE_H__
#define __FLAMINGS_MEMORY_RED_BLACK_TREE_H__

struct MemoryRedBlackTree;
struct MemoryRedBlackTreeIterator;
struct MemoryRedBlackTreeNode;
struct MemoryRedBlackTreeNodeBlock;

enum MemoryRedBlackTreeColor {
    KMRBT_UNUSED,
    KMRBT_RED,
    KMRBT_BLACK
};

struct MemoryRedBlackTreeIterator {
    struct MemoryRedBlackTreeNode *current;
    bool forward;
};

struct MemoryRedBlackTree {
    struct MemoryRedBlackTreeNodeBlock *head_block;
    struct MemoryRedBlackTreeNodeBlock *tail_block;
    struct MemoryRedBlackTreeNode *root;
};

struct MemoryRedBlackTreeNode {
    struct MemoryRedBlackTreeNode *parent;
    struct MemoryRedBlackTreeNode *left;
    struct MemoryRedBlackTreeNode *right;
    enum MemoryRedBlackTreeColor color;
    size_t offset;
    size_t length;
};

/**
 * Used for convenience by unit tests
 */
struct MemoryRedBlackTreeNodeBlockBase {
    struct MemoryRedBlackTreeNodeBlock *next;
    size_t length;
};

struct MemoryRedBlackTreeNodeBlock {
    struct MemoryRedBlackTreeNodeBlock *next;
    size_t length;
    struct MemoryRedBlackTreeNode nodes[];
};

EXTERN_C_START
    enum FError f_mrbt_init(struct MemoryRedBlackTree *self);
    enum FError f_mrbt_add_allocation(struct MemoryRedBlackTree *self, struct MemoryRedBlackTreeNodeBlock *block);
    enum FError f_mrbt_insert_element(struct MemoryRedBlackTree *self, size_t offset, size_t length);
    enum FError f_mrbt_find_element(struct MemoryRedBlackTree *self, size_t address, struct MemoryRedBlackTreeIterator *iterator);
    enum FError f_mrbt_remove_element(struct MemoryRedBlackTree *self, struct MemoryRedBlackTreeIterator *iterator);
    enum FError f_mrbt_get_iterator(struct MemoryRedBlackTree *self, struct MemoryRedBlackTreeIterator *result);

    enum FError f_mrbti_has_elements(struct MemoryRedBlackTreeIterator *self, bool *result);
    enum FError f_mrbti_move_next(struct MemoryRedBlackTreeIterator *self);
    enum FError f_mrbti_clone(struct MemoryRedBlackTreeIterator *self, struct MemoryRedBlackTreeIterator *result);
    enum FError f_mrbti_clone_reversed(struct MemoryRedBlackTreeIterator *self, struct MemoryRedBlackTreeIterator *result);
    enum FError f_mrbti_get_offset(struct MemoryRedBlackTreeIterator *self, size_t *result);
    enum FError f_mrbti_get_length(struct MemoryRedBlackTreeIterator *self, size_t *result);
    enum FError f_mrbti_get_color(struct MemoryRedBlackTreeIterator *self, enum MemoryRedBlackTreeColor *result);
    enum FError f_mrbti_set_offset(struct MemoryRedBlackTreeIterator *self, size_t offset);
    enum FError f_mrbti_set_length(struct MemoryRedBlackTreeIterator *self, size_t length);
EXTERN_C_END

#endif // __FLAMINGS_MEMORY_RED_BLACK_TREE_H__
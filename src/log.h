#ifndef FLAMINGOS_LOG_H
#define FLAMINGOS_LOG_H

/**
 * Trace messages should be logged when we want to trace
 * the flow of the program.  This is the most fine-grained
 * level of logging.
 */
#define F_LOGLEVEL_TRACE  0

/**
 * Debug messges should be logged when we need to see what
 * the system is doing but we don't need to trace the program
 * flow.
 */
#define F_LOGLEVEL_DEBUG  1

/**
 * Informational messages should be logged when we just want
 * to report normal program flow, e.g. serivce turned on/turned off
 */
#define F_LOGLEVEL_INFO   2

/**
 * Warning messages should be logged when we want to report
 * an abnormal situation from which the operation could recover.
 * This can also warn of situations which are not errors, but
 * can be problematic at some point in the future (e.g.
 * invoking a deprecated function).
 */
#define F_LOGLEVEL_WARN   3

/**
 * Error messages should be logged when we have encountered
 * an abnormal situation that is fatal to the operation but
 * the kernel can recover from it (e.g. a segmentation fault
 * in a running application).
 */
#define F_LOGLEVEL_ERROR  4

/**
 * Error messages should be logged when we have encountered
 * an abnormal situation, and the kernel cannot recover from
 * it and must shut down immediately.
 */
#define F_LOGLEVEL_FATAL  5

#ifndef F_LOG_LEVEL
#define F_LOG_LEVEL F_LOGLEVEL_INFO
#endif

#if F_LOG_LEVEL < F_LOGLEVEL_TRACE || F_LOG_LEVEL > F_LOGLEVEL_FATAL
#error "Invalid value for F_LOG_LEVEL"
#endif

#if F_LOG_LEVEL <= F_LOGLEVEL_TRACE
#define F_LOG_TRACE(message, ...) f_log(F_LOGLEVEL_TRACE, (message), __FILE__, __LINE__ __VA_OPT__(,) __VA_ARGS__)
#else
#define F_LOG_TRACE(message, ...)
#endif

#if F_LOG_LEVEL <= F_LOGLEVEL_DEBUG
#define F_LOG_DEBUG(message) f_log(F_LOGLEVEL_DEBUG, (message), __FILE__, __LINE__ __VA_OPT__(,) __VA_ARGS__)
#else
#define F_LOG_DEBUG(message)
#endif

#if F_LOG_LEVEL <= F_LOGLEVEL_INFO
#define F_LOG_INFO(message, ...) f_log(F_LOGLEVEL_INFO, (message), __FILE__, __LINE__ __VA_OPT__(,) __VA_ARGS__)
#else
#define F_LOG_INFO(message, ...)
#endif

#if F_LOG_LEVEL <= F_LOGLEVEL_WARN
#define F_LOG_WARN(message, ...) f_log(F_LOGLEVEL_WARN, (message), __FILE__, __LINE__ __VA_OPT__(,) __VA_ARGS__)
#else
#define F_LOG_WARN(message)
#endif

#if F_LOG_LEVEL <= F_LOGLEVEL_ERROR
#define F_LOG_ERROR(message, ...) f_log(F_LOGLEVEL_ERROR, (message), __FILE__, __LINE__ __VA_OPT__(,) __VA_ARGS__)
#else
#define F_LOG_ERROR(message)
#endif

#if F_LOG_LEVEL <= F_LOGLEVEL_FATAL
#define F_LOG_FATAL(message, ...) f_log(F_LOGLEVEL_FATAL, (message), __FILE__, __LINE__ __VA_OPT__(,) __VA_ARGS__)
#else
#define F_LOG_FATAL(message)
#endif

/**
 * \brief Log a message to the serial port
 * 
 * Logging to the serial port is better than logging to the terminal
 * for a number of reasons:
 * 
 *  - The data sent to the serial port can be redirected to a file on the host machine,
 *    allowing for us to see ALL of the logs, and not just the logs that haven't been
 *    replaced on screen.
 * 
 *  - The screen may have a bug, especially when we move from the deprecated text mode
 *    to a graphical mode terminal.
 * 
 *  - The data from the serial port can be studied even when the virtual machine is off.
 * 
 * \param logLevel The log level at which the message should be recorded.
 * \param message  The message to be logged
 * \param fileName The name of the file from which the message came
 * \param lineNumber The line of the file from which the message came
 * 
 * The implementation of this logger MUST NOT USE THE HEAP!  As of the writing of this
 * documentation, memory management has not been implemented yet, and the eventual
 * implementation will likely use this very logger.  A kernel cannot afford to have a situation
 * where we try to log an error in the memory manager, the logger tries to allocate memory,
 * then the memory manager tries to log the same error, etc.
 * 
 * TODO Add the date and time to the logger
 */
EXTERN_C void f_log(int logLevel, const char *message, const char *fileName, size_t lineNumber, ...);

/**
 * \brief The function to be used by the logger
 * 
 * Any function that matches this signature must not use the heap.
 * Doing so can caus an infinite loop becuase the memory manager
 * will likely use the logger.
 * 
 * \param s        The string to write
 * \param count    The number of characters to write
 * \param finished Weather this is the last invocation for this log message
 * \returns The number of characters written
 */
typedef int (*FLogCallback)(const char *s, int count, bool finished);

/**
 * \brief Set the callback to receive the logs
 */
EXTERN_C void f_log_callback(FLogCallback callback);

#endif // FLAMINGOS_LOG_H
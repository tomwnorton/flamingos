#ifndef FLAMINGOS_SERIAL_H
#define FLAMINGOS_SERIAL_H

EXTERN_C_START
bool f_serial_init(void);
uint8_t f_serial_read(void);
void f_serial_write_byte(uint8_t b);
int f_serial_write_string(const char *s);
EXTERN_C_END

#endif // FLAMINGOS_SERIAL_H
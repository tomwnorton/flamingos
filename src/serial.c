#include <stdint.h>
#include <stdbool.h>
#include "cxx.h"
#include "pic-io.h"

#define PORT_COM1 0x03F8
#define PORT_COM2 0x02F8
#define PORT_COM3 0x03E8
#define PORT_COM4 0x02E8

#define COM_REGISTER_DATA                   0
#define COM_REGISTER_INTERRUPT_ENABLE       1
#define COM_REGISTER_INTURRUPT_FIFO_CONTROL 2
#define COM_REGISTER_LINE_CONTROL           3
#define COM_REGISTER_MODEM_CONTROL          4
#define COM_REGISTER_LINE_STATUS            5
#define COM_REGISTER_MODEM_STATUS           6
#define COM_REGISTER_SCRATCH                7

void terminal_writestring(const char* data);
void terminal_writehex(unsigned int hex);


bool f_serial_init(void) {
    f_outb(PORT_COM1 + COM_REGISTER_INTERRUPT_ENABLE, 0);       // Disable all interrupts
    f_outb(PORT_COM1 + COM_REGISTER_LINE_CONTROL, 0x80);        // Enable DLAB
    f_outb(PORT_COM1 + COM_REGISTER_DATA, 0x03);                // Set divisor to 3 (lo byte), 38400 Baud
    f_outb(PORT_COM1 + COM_REGISTER_INTERRUPT_ENABLE, 0x00);    //                  (hi byte)
    f_outb(PORT_COM1 + COM_REGISTER_LINE_CONTROL, 0x03);        // 8 bits, no parity, 1 stop bit
    f_outb(PORT_COM1 + COM_REGISTER_INTURRUPT_FIFO_CONTROL, 0xC7); // Enable FIFO, clear them, with 14-byte threshold
    f_outb(PORT_COM1 + COM_REGISTER_MODEM_CONTROL, 0x0B);       // IRQs enabled, RTS/DSR set

    // This check isn't working.  It fails even though the serial port is up
    //f_outb(PORT_COM1 + COM_REGISTER_MODEM_CONTROL, 0x1E);       // Set in loopback mode, test the serial chip
    //f_outb(PORT_COM1 + COM_REGISTER_DATA, 0xAE);                // Send test byte to loopback
    //uint8_t res = f_inb(PORT_COM1 + COM_REGISTER_DATA);
    //if (res != 0xAE) return false;

    f_outb(PORT_COM1 + COM_REGISTER_MODEM_CONTROL, 0x0F);       // Ready to read for real
    return true;
}

bool f_serial_has_bytes_ready(void) {
    return f_inb(PORT_COM1 + COM_REGISTER_LINE_STATUS) & 0x01;
}

bool f_serial_transmit_empty(void) {
    return f_inb(PORT_COM1 + COM_REGISTER_LINE_STATUS) & 0x20;
}

uint8_t f_serial_read(void) {
    while (!f_serial_has_bytes_ready());
    return f_inb(PORT_COM1 + COM_REGISTER_DATA);
}

void f_serial_write_byte(uint8_t b) {
    while (!f_serial_transmit_empty());
    f_outb(PORT_COM1 + COM_REGISTER_DATA, b);
}

int f_serial_write_string(const char *s) {
    int length = 0;
    while (*s != '\0') {
        f_serial_write_byte(*s);
        ++s;
        ++length;
    }
    return length;
}
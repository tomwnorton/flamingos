#ifndef FLAMINGOS_GDT_H
#define FLAMINGOS_GDT_H

#include "cxx.h"

struct GDTDescriptor {
    uint16_t length;
    uint32_t offset;
};

struct GDTEntry {
    uint16_t limit_lower;
    uint32_t base_lower: 24;
    uint8_t access_byte;
    uint8_t limit_upper: 4;
    uint8_t flags: 4;
    uint8_t base_upper;
} __attribute__((packed));

struct GDTAccessByte {
    bool accessed : 1;
    bool readable_writable : 1;
    bool direction_conforming : 1;
    bool executable : 1;
    bool non_system_segment: 1;
    uint8_t privilege : 2;
    bool present : 1;
} __attribute__((packed));

struct GDTFlags {
    bool reserved : 1;
    bool long_mode : 1;
    bool size : 1;
    bool granularity : 1;
    uint8_t more_reserved : 4;
} __attribute__((packed));

enum GDTSegmentType {
    GDT_SEGMENT_TYPE_NULL,
    GDT_SEGMENT_TYPE_CODE_EXECUTE_READ,
    GDT_SEGMENT_TYPE_CODE_EXECUTE_ONLY,
    GDT_SEGMENT_TYPE_DATA_READ_WRITE,
    GDT_SEGMENT_TYPE_DATA_READ_ONLY,
    GDT_SEGMENT_TYPE_TASK_STATE
};

enum GDTGranularity {
    GDT_GRANULARITY_BYTES,
    GDT_GRANULARITY_PAGES
};

struct GDTEntryModel {
    uint32_t base;
    uint32_t limit;
    uint8_t type;
    uint8_t granularity;
};

EXTERN_C_START
bool f_populate_gdt_entries(struct GDTEntryModel *models, struct GDTEntryModel *modelsEnd, struct GDTEntry *entries);
bool f_setup_gdt(void);
void f_load_gdt(struct GDTEntry *entries, uint16_t sizeInBytes);
void f_reload_segments(void);
EXTERN_C_END

#endif //FLAMINGOS_GDT_H

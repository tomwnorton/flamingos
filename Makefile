LOGLEVEL_TRACE:=0
LOGLEVEL_DEBUG:=1
LOGLEVEL_INFO:=2
LOGLEVEL_WARN:=3
LOGLEVEL_ERROR:=4
LOGLEVEL_FATAL:=5
LOG_LEVEL:=$(LOGLEVEL_INFO)

HOST_CXX:=g++
HOST_CC:=gcc
CC:=i686-elf-gcc
CXX:=i686-elf-g++
AS:=i686-elf-as
GRUB_MKRESCUE:=$(shell bash get-grub-mkrescue-command.sh)
TEST_CXX_FLAGS:=-m32 -c -g --std=gnu++14 -iquote src -iquote include -iquote test -iquote test -isystem third-party/Catch2/single_include -DTEST_RUN
CXX_FLAGS:=-c --std=gnu++14 -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti -iquote src -DF_LOG_LEVEL=$(LOG_LEVEL)
CFLAGS:=-c --std=gnu11 -ffreestanding -O2 -Wall -Wextra -Wno-missing-braces -iquote src -DF_LOG_LEVEL=$(LOG_LEVEL)
TEST_CFLAGS:=-m32 -g $(CFLAGS) -DTEST_RUN
TEST_COMPILE:=$(HOST_CXX) $(TEST_CXX_FLAGS)
CRTBEGIN_OBJ:=$(shell $(CC) $(CFLAGS) -print-file-name=crtbegin.o)
CRTEND_OBJ:=$(shell $(CC) $(CFLAGS) -print-file-name=crtend.o)
FLAMINGOS_OBJ_TEST:=host-bin/abort.o \
					host-bin/test-main.o \
					host-bin/memory-red-black-tree.o \
					host-bin/gdt.o host-bin/log.o \
					host-bin/memory-red-black-tree-test.o \
					host-bin/gdt-test.o \
					host-bin/log-test.o \
					host-bin/string.o \
					host-bin/setup-paging.o \
					host-bin/setup-paging-test.o \
					host-bin/memory-multiboot-memory-map-iterator.o \
					host-bin/memory-multiboot-memory-map-iterator-test.o
FLAMINGOS_OBJ_INNER:=bin/cxx-runtime.o \
					 bin/kernel.o \
					 bin/memory-red-black-tree.o \
					 bin/load-gdt.o \
					 bin/gdt.o \
					 bin/reload-segments.o \
					 bin/pic-io.o \
					 bin/serial.o \
					 bin/log.o \
					 bin/string.o \
					 bin/setup-paging.o \
					 bin/memory-multiboot-memory-map-iterator.o
FLAMINGOS_OBJ:= bin/boot.o bin/crti.o $(CRTBEGIN_OBJ) $(FLAMINGOS_OBJ_INNER) $(CRTEND_OBJ) bin/crtn.o

all: run-all-tests bin/flamingos.iso

clean: clean-test clean-flamingos

clean-test:
	rm -rf host-bin/*

clean-flamingos:
	rm -rf bin/*

run-all-tests: all-tests
	host-bin/core-test

all-tests: host-bin/core-test

host-bin/core-test: $(FLAMINGOS_OBJ_TEST)
	$(HOST_CXX) -m32 -g $(FLAMINGOS_OBJ_TEST) -o$@

host-bin/abort.o: test/abort.c
	$(HOST_CC) $(TEST_CFLAGS) $< -o$@

host-bin/test-main.o: test/test-main.cxx
	$(HOST_CXX) $(TEST_CXX_FLAGS) $< -o$@

host-bin/memory-red-black-tree.o: src/memory/red-black-tree.c src/memory/red-black-tree.h
	$(HOST_CC) $(TEST_CFLAGS) $< -o$@

host-bin/memory-red-black-tree-test.o: test/memory/red-black-tree-test.cxx test/memory/red-black-tree-matchers.hxx src/memory/red-black-tree.h
	$(HOST_CXX) $(TEST_CXX_FLAGS) $< -o$@

host-bin/gdt.o: src/gdt.c src/gdt.h
	$(HOST_CC) $(TEST_CFLAGS) $< -o$@

host-bin/gdt-test.o: test/gdt-test.cxx src/gdt.h
	$(HOST_CXX) $(TEST_CXX_FLAGS) $< -o$@

host-bin/log.o: src/log.c src/log.h src/string.h
	$(HOST_CXX) $(TEST_CXX_FLAGS) $< -o$@

host-bin/log-test.o: test/log-test.cxx src/log.h
	$(HOST_CXX) $(TEST_CXX_FLAGS) $< -o$@

host-bin/string.o: src/string.c src/string.h
	$(HOST_CC) $(TEST_CFLAGS) $< -o$@

host-bin/memory-multiboot-memory-map-iterator.o: src/memory/multiboot-memory-map-iterator.c src/memory/multiboot-memory-map-iterator.h
	$(HOST_CC) $(TEST_CFLAGS) $< -o$@

host-bin/memory-multiboot-memory-map-iterator-test.o: test/memory/multiboot-memory-map-iterator-test.cxx src/memory/multiboot-memory-map-iterator.h
	$(HOST_CXX) $(TEST_CXX_FLAGS) $< -o$@

host-bin/setup-paging.o: src/memory/setup-paging.c
	$(HOST_CC) $(TEST_CFLAGS) $< -o$@

host-bin/setup-paging-test.o: test/memory/setup-paging-test.cxx
	$(HOST_CXX) $(TEST_CXX_FLAGS) $< -o$@

bin/boot.o: src/boot.S
	$(AS) $< -o $@

bin/load-gdt.o: src/load-gdt.S
	$(AS) $< -o $@

bin/reload-segments.o: src/reload-segments.S
	$(AS) $< -o $@

bin/pic-io.o: src/pic-io.S
	$(AS) $< -o $@

bin/serial.o: src/serial.c src/serial.h src/pic-io.h
	$(CC) $(CFLAGS) $< -o$@

bin/string.o: src/string.c src/string.h
	$(CC) $(CFLAGS) $< -o$@

bin/log.o: src/log.c src/log.h src/string.h
	$(CC) $(CFLAGS) $< -o$@

bin/kernel.o: src/kernel.c src/serial.h src/log.h src/multiboot.h
	$(CC) $(CFLAGS) $< -o$@

bin/cxx-runtime.o: src/cxx-runtime.cxx
	$(CXX) $(CXX_FLAGS) $< -o$@

bin/memory-red-black-tree.o: src/memory/red-black-tree.c src/memory/red-black-tree.h
	$(CC) $(CFLAGS) $< -o$@

bin/gdt.o: src/gdt.c src/gdt.h
	$(CC) $(CFLAGS) $< -o$@

bin/memory-multiboot-memory-map-iterator.o: src/memory/multiboot-memory-map-iterator.c src/memory/multiboot-memory-map-iterator.h
	$(CC) $(CFLAGS) $< -o$@

bin/setup-paging.o: src/memory/setup-paging.c
	$(CC) $(CFLAGS) $< -o$@

bin/flamingos.bin: src/linker.ld $(FLAMINGOS_OBJ)
	$(CC) -T src/linker.ld -o $@ -ffreestanding -O2 -nostdlib $(FLAMINGOS_OBJ) -lgcc
	./assert-multiboot

bin/crti.o: src/crti.S
	$(AS) $< -o $@

bin/crtn.o: src/crtn.S
	$(AS) $< -o $@

bin/flamingos.iso: bin/flamingos.bin src/grub.cfg
	rm -rf bin/isodir
	mkdir -p bin/isodir/boot/grub
	cp bin/flamingos.bin bin/isodir/boot/flamingos.bin
	cp src/grub.cfg bin/isodir/boot/grub/grub.cfg
	$(GRUB_MKRESCUE) -o $@ bin/isodir
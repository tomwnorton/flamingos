#!/usr/bin/env bash
if command -v grub2-mkrescue > /dev/null 2>&1 ; then
  echo -n "grub2-mkrescue"
else
  echo -n "grub-mkrescue"
fi
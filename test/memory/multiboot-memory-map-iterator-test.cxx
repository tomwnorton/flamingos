#include <catch2/catch.hpp>
#include <cstdint>
#include "cxx.h"
#include "kerror.h"
#include "memory/multiboot-memory-map-iterator.h"
#include "multiboot.h"

TEST_CASE("mmap_iterator_has_next can't be called with invalid arguments") {
    struct MultibootMemoryMapIterator iterator = {0};
    bool result;

    REQUIRE(mmap_iterator_has_next(&iterator, nullptr) == FERROR_NULL_REFERENCE);
    REQUIRE(mmap_iterator_has_next(nullptr, &result) == FERROR_NULL_REFERENCE);
}

TEST_CASE("mmap_iterator_move_next can't be called with invalid arguments") {
    REQUIRE(mmap_iterator_move_next(nullptr) == FERROR_NULL_REFERENCE);
}

TEST_CASE("mmap_iterator_get_start_address can't be called with invalid arguments") {
    struct MultibootMemoryMapIterator iterator = {0};
    unsigned long long result;

    REQUIRE(mmap_iterator_get_start_address(&iterator, nullptr) == FERROR_NULL_REFERENCE);
    REQUIRE(mmap_iterator_get_start_address(nullptr, &result) == FERROR_NULL_REFERENCE);
}

TEST_CASE("mmap_iterator_get_length can't be called with invalid arguments") {
    struct MultibootMemoryMapIterator iterator = {0};
    unsigned long long result;

    REQUIRE(mmap_iterator_get_length(&iterator, nullptr) == FERROR_NULL_REFERENCE);
    REQUIRE(mmap_iterator_get_length(nullptr, &result) == FERROR_NULL_REFERENCE);
}

TEST_CASE("mmap_iterator_is_usable can't be called with invalid arguments") {
    MultibootMemoryMapIterator iterator = {0};
    bool result;

    REQUIRE(mmap_iterator_is_usable(&iterator, nullptr) == FERROR_NULL_REFERENCE);
    REQUIRE(mmap_iterator_is_usable(nullptr, &result) == FERROR_NULL_REFERENCE);
}

TEST_CASE("A memory map iterator can be iterated if it is empty") {
    struct MultibootMemoryMapEntry entry = {0};
    struct MultibootMemoryMapIterator iterator = { 0UL,  reinterpret_cast<size_t>(&entry)};
    bool result = true;

    REQUIRE(mmap_iterator_has_next(&iterator, &result) == FERROR_OK);
    REQUIRE_FALSE(result);
}

TEST_CASE("A memory map iterator can go through a list with a single element") {
    std::uint8_t buffer[80UL] __attribute__((aligned(4UL)));
    auto entry = reinterpret_cast<MultibootMemoryMapEntry*>(buffer);
    entry->size = 20UL;
    entry->base_addr_high = 0x12345678UL;
    entry->base_addr_low =  0x9ABCDEF0UL;
    entry->length_high =    0xFEDCBA98UL;
    entry->length_low =     0x76543210UL;
    entry->type = 1UL;

    entry = reinterpret_cast<MultibootMemoryMapEntry*>(buffer + 24UL);
    entry->size = 28UL;
    entry->base_addr_high =  0x9ABCDEF0UL;
    entry->base_addr_low = 0x12345678UL;
    entry->length_high =     0x76543210UL;
    entry->length_low =    0xFEDCBA98UL;
    entry->type = 3UL;

    entry = reinterpret_cast<MultibootMemoryMapEntry*>(buffer + 56UL);
    entry->size = 20UL;
    entry->base_addr_high =  0;
    entry->base_addr_low = 0xDEADBEEFUL;
    entry->length_high =     0;
    entry->length_low =    0xCAFEBABEUL;
    entry->type = 2UL;

    MultibootMemoryMapIterator iterator = { 80UL, reinterpret_cast<size_t>(buffer) };
    unsigned long long startAddress = 0ULL;
    unsigned long long length = 0ULL;
    bool hasNext = false;
    bool isUsable = false;

    // First Iteration
    CHECK(mmap_iterator_has_next(&iterator, &hasNext) == FERROR_OK);
    CHECK(hasNext);

    CHECK(mmap_iterator_get_start_address(&iterator, &startAddress) == FERROR_OK);
    CHECK(startAddress == 0x123456789ABCDEF0ULL);

    CHECK(mmap_iterator_get_length(&iterator, &length) == FERROR_OK);
    CHECK(length == 0xFEDCBA9876543210ULL);

    CHECK(mmap_iterator_is_usable(&iterator, &isUsable) == FERROR_OK);
    CHECK(isUsable);

    CHECK(mmap_iterator_move_next(&iterator) == FERROR_OK);

    // Second Iteration
    CHECK(mmap_iterator_has_next(&iterator, &hasNext) == FERROR_OK);
    CHECK(hasNext);

    CHECK(mmap_iterator_get_start_address(&iterator, &startAddress) == FERROR_OK);
    CHECK(startAddress == 0x9ABCDEF012345678ULL);

    CHECK(mmap_iterator_get_length(&iterator, &length) == FERROR_OK);
    CHECK(length == 0x76543210FEDCBA98ULL);

    CHECK(mmap_iterator_is_usable(&iterator, &isUsable) == FERROR_OK);
    CHECK_FALSE(isUsable);

    CHECK(mmap_iterator_move_next(&iterator) == FERROR_OK);

    // Third Iteration
    CHECK(mmap_iterator_has_next(&iterator, &hasNext) == FERROR_OK);
    CHECK(hasNext);

    CHECK(mmap_iterator_get_start_address(&iterator, &startAddress) == FERROR_OK);
    CHECK(startAddress == 0xDEADBEEFULL);

    CHECK(mmap_iterator_get_length(&iterator, &length) == FERROR_OK);
    CHECK(length == 0xCAFEBABEULL);

    CHECK(mmap_iterator_is_usable(&iterator, &isUsable) == FERROR_OK);
    CHECK_FALSE(isUsable);

    CHECK(mmap_iterator_move_next(&iterator) == FERROR_OK);

    // After the last iteration
    CHECK(mmap_iterator_has_next(&iterator, &hasNext) == FERROR_OK);
    CHECK_FALSE(hasNext);

    CHECK(mmap_iterator_get_start_address(&iterator, &startAddress) == FERROR_ELEMENT_NOT_FOUND);
    CHECK(mmap_iterator_get_length(&iterator, &length) == FERROR_ELEMENT_NOT_FOUND);
    CHECK(mmap_iterator_is_usable(&iterator, &isUsable) == FERROR_ELEMENT_NOT_FOUND);

    CHECK(mmap_iterator_move_next(&iterator) == FERROR_ELEMENT_NOT_FOUND);

    CHECK(mmap_iterator_has_next(&iterator, &hasNext) == FERROR_OK);
    CHECK_FALSE(hasNext);
}
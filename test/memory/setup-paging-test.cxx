#include <cstdint>
#include <catch2/catch.hpp>

extern "C" void f_setup_paging(size_t pageDirectory[1024],
                               size_t firstPageTable[1024],
                               size_t secondPageTable[1024],
                               uint8_t* kernelStart,
                               uint8_t* writableDataStart);

size_t pageDirectory[1024]   __attribute__((aligned(0x1000)));
size_t firstPageTable[1024]  __attribute__((aligned(0x1000)));
size_t secondPageTable[1024] __attribute__((aligned(0x1000)));

uint8_t* const KERNEL_START          = reinterpret_cast<uint8_t*>(0x00104000UL);
uint8_t* const KERNEL_WRITABLE_START = reinterpret_cast<uint8_t*>(0x0010C000UL);

TEST_CASE("Paging can be set up") {
    f_setup_paging(pageDirectory, firstPageTable, secondPageTable, KERNEL_START, KERNEL_WRITABLE_START);

    SECTION("The first megabyte is identity pages only") {
        size_t* actualPageTableAddress = reinterpret_cast<size_t*>(pageDirectory[0] & 0xFFFFF000UL);
        CHECK(firstPageTable == actualPageTableAddress);
        CHECK(pageDirectory[0] & 0x03UL);
        CHECK(firstPageTable[0]    == 0x00000003UL);
        CHECK(firstPageTable[1]    == 0x00001003UL);
        CHECK(firstPageTable[2]    == 0x00002003UL);
        CHECK(firstPageTable[3]    == 0x00003003UL);
        CHECK(firstPageTable[255]  == 0x000FF003UL);
        CHECK(firstPageTable[256]  == 0x00100003UL);
        CHECK(firstPageTable[257]  == 0x00101003UL);
        CHECK(firstPageTable[258]  == 0x00102003UL);
        CHECK(firstPageTable[259]  == 0x00103003UL);
        CHECK(firstPageTable[260]  == 0UL);
        CHECK(firstPageTable[1023] == 0UL);
    }

    SECTION("The kernel is mapped to 3GiB") {
        size_t* actualPageTableAddress = reinterpret_cast<size_t*>(pageDirectory[768] & 0xFFFFF000UL);
        CHECK(secondPageTable == actualPageTableAddress);
        CHECK(pageDirectory[768] & 0x03UL);
        CHECK(secondPageTable[0] == 0x00104001UL);
        CHECK(secondPageTable[1] == 0x00105001UL);
        CHECK(secondPageTable[2] == 0x00106001UL);
        CHECK(secondPageTable[3] == 0x00107001UL);
        CHECK(secondPageTable[4] == 0x00108001UL);
        CHECK(secondPageTable[5] == 0x00109001UL);
        CHECK(secondPageTable[6] == 0x0010A001UL);
        CHECK(secondPageTable[7] == 0x0010B001UL);
        CHECK(secondPageTable[8] == 0x0010C003UL);
        CHECK(secondPageTable[9] == 0x0010D003UL);
        CHECK(secondPageTable[10] == 0x0010E003UL);
        CHECK(secondPageTable[763] == 0x003FF003UL);
    }
}
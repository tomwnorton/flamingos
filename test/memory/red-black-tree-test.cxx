#include <catch2/catch.hpp>
#include <cstdint>
#include <sstream>
#include <vector>
#include <memory>
#include <iostream>
#include <stdexcept>
#include "cxx.h"
#include "kerror.h"
#include "memory/red-black-tree.h"
#include "memory/red-black-tree-internal.h"
#include "red-black-tree-matchers.hxx"

TEST_CASE("Red/Black Tree functions can't accept NULL for any parameter") {
    MemoryRedBlackTree tree;
    MemoryRedBlackTreeIterator iterator;
    MemoryRedBlackTreeNodeBlock allocation;
    MemoryRedBlackTreeColor color;
    bool boolResult;
    size_t sizeResult;

    CHECK(FERROR_NULL_REFERENCE == f_mrbt_init(nullptr));
    CHECK(FERROR_NULL_REFERENCE == f_mrbt_insert_element(nullptr, 0, 0));

    CHECK(FERROR_NULL_REFERENCE == f_mrbt_add_allocation(&tree, nullptr));
    CHECK(FERROR_NULL_REFERENCE == f_mrbt_add_allocation(nullptr, &allocation));

    CHECK(FERROR_NULL_REFERENCE == f_mrbt_get_iterator(&tree, nullptr));
    CHECK(FERROR_NULL_REFERENCE == f_mrbt_get_iterator(nullptr, &iterator));

    CHECK(FERROR_NULL_REFERENCE == f_mrbti_has_elements(&iterator, nullptr));
    CHECK(FERROR_NULL_REFERENCE == f_mrbti_has_elements(nullptr, &boolResult));

    CHECK(FERROR_NULL_REFERENCE == f_mrbti_move_next(nullptr));

    CHECK(FERROR_NULL_REFERENCE == f_mrbti_clone(&iterator, nullptr));
    CHECK(FERROR_NULL_REFERENCE == f_mrbti_clone(nullptr, &iterator));

    CHECK(FERROR_NULL_REFERENCE == f_mrbti_clone_reversed(&iterator, nullptr));
    CHECK(FERROR_NULL_REFERENCE == f_mrbti_clone_reversed(nullptr, &iterator));

    CHECK(FERROR_NULL_REFERENCE == f_mrbti_get_offset(&iterator, nullptr));
    CHECK(FERROR_NULL_REFERENCE == f_mrbti_get_offset(nullptr, &sizeResult));

    CHECK(FERROR_NULL_REFERENCE == f_mrbti_get_length(&iterator, nullptr));
    CHECK(FERROR_NULL_REFERENCE == f_mrbti_get_length(nullptr, &sizeResult));

    CHECK(FERROR_NULL_REFERENCE == f_mrbti_get_color(&iterator, nullptr));
    CHECK(FERROR_NULL_REFERENCE == f_mrbti_get_color(nullptr, &color));

    CHECK(FERROR_NULL_REFERENCE == f_mrbti_set_offset(nullptr, 0UL));
    CHECK(FERROR_NULL_REFERENCE == f_mrbti_set_length(nullptr, 0UL));
}

TEST_CASE("A Red/Black Tree can allocate memory for new nodes") {
    typedef std::unique_ptr<MemoryRedBlackTreeNodeBlock, typeof(&destroy_node_block)> NodeBlockUniquePtr;
    MemoryRedBlackTree tree;
    REQUIRE(FERROR_OK == f_mrbt_init(&tree));

    MemoryRedBlackTreeNode dummy;
    std::vector<uint8_t> firstBuffer(sizeof(MemoryRedBlackTreeNodeBlock) + sizeof(MemoryRedBlackTreeNode) * 8UL);
    NodeBlockUniquePtr firstBlock(new (firstBuffer.data()) MemoryRedBlackTreeNodeBlock, destroy_node_block);
    firstBlock->nodes->left = &dummy;
    firstBlock->nodes->right = &dummy;
    firstBlock->length = 8UL;

    std::vector<uint8_t> secondBuffer(sizeof(MemoryRedBlackTreeNodeBlock) + sizeof(MemoryRedBlackTreeNode) * 3UL);
    NodeBlockUniquePtr secondBlock(new (secondBuffer.data()) MemoryRedBlackTreeNodeBlock, destroy_node_block);
    secondBlock->length = 3UL;

    MemoryRedBlackTreeNode *node = nullptr;
    REQUIRE(FERROR_OUT_OF_MEMORY == f_mrbt_create_node(&tree, &node, 0x400, 0x80));

    REQUIRE(FERROR_OK == f_mrbt_add_allocation(&tree, firstBlock.get()));
    REQUIRE_THAT(tree,
                 HasAllocatedBlocks("UNUSED\n"
                      "UNUSED\n"
                      "UNUSED\n"
                      "UNUSED\n"
                      "UNUSED\n"
                      "UNUSED\n"
                      "UNUSED\n"
                      "UNUSED\n"));

    REQUIRE(FERROR_OK == f_mrbt_create_node(&tree, &node, 0x400, 0x80));
    REQUIRE(node == firstBlock->nodes);
    REQUIRE(node->left == nullptr);
    REQUIRE(node->right == nullptr);
    REQUIRE_THAT(tree,
                 HasAllocatedBlocks("400 80 RED\n"
                      "UNUSED\n"
                      "UNUSED\n"
                      "UNUSED\n"
                      "UNUSED\n"
                      "UNUSED\n"
                      "UNUSED\n"
                      "UNUSED\n"));

    REQUIRE(FERROR_OK == f_mrbt_create_node(&tree, &node, 0x100, 0x10));
    REQUIRE(node == firstBlock->nodes + 1);
    REQUIRE_THAT(tree,
                 HasAllocatedBlocks("400 80 RED\n"
                      "100 10 RED\n"
                      "UNUSED\n"
                      "UNUSED\n"
                      "UNUSED\n"
                      "UNUSED\n"
                      "UNUSED\n"
                      "UNUSED\n"));

    for (int i = 2; i < 8; ++i) {
        REQUIRE(FERROR_OK == f_mrbt_create_node(&tree, &node, 0x480, 0x280));
        REQUIRE(node == firstBlock->nodes + i);
    }

    REQUIRE(FERROR_OUT_OF_MEMORY == f_mrbt_create_node(&tree, &node, 0x100, 0xABC));

    REQUIRE(FERROR_OK == f_mrbt_add_allocation(&tree, secondBlock.get()));
    REQUIRE_THAT(tree,HasAllocatedBlocks(
            "400  80 RED\n"
            "100  10 RED\n"
            "480 280 RED\n"
            "480 280 RED\n"
            "480 280 RED\n"
            "480 280 RED\n"
            "480 280 RED\n"
            "480 280 RED\n"
            "UNUSED\n"
            "UNUSED\n"
            "UNUSED\n"));

    REQUIRE(FERROR_OK == f_mrbt_create_node(&tree, &node, 0x100, 0xABC));
}

TEST_CASE("Add ability to insert nodes into a tree") {
    MemoryRedBlackTree tree;
    REQUIRE(FERROR_OK == f_mrbt_init(&tree));

    SECTION("Add ability to add a root node") {
        MemoryRedBlackTreeNode node, expectedLeft, expectedRight;
        from_string("400 80 RED", node);
        from_string("100 80 RED", expectedLeft);
        from_string("800 80 RED", expectedRight);

        REQUIRE(FERROR_OK == f_mrbt_insert_node(&tree, &node));
        REQUIRE_THAT(tree, MatchesTreeStructure("400 80 RED"));

        SECTION("it can insert a node to the left of the root") {
            REQUIRE(FERROR_OK == f_mrbt_insert_node(&tree, &expectedLeft));
            REQUIRE_THAT(tree, MatchesTreeStructure("400 80 RED\n"
                                                    "  100 80 RED\n"));

            SECTION("it can insert a node on lower levels") {
                MemoryRedBlackTreeNode otherLeft, otherRight;
                from_string("40  80 RED", otherLeft);
                from_string("200 80 RED", otherRight);
                REQUIRE(FERROR_OK == f_mrbt_insert_node(&tree, &otherLeft));
                REQUIRE(FERROR_OK == f_mrbt_insert_node(&tree, &otherRight));
                REQUIRE_THAT(tree, MatchesTreeStructure("400 80 RED\n"
                                                        "  100 80 RED\n"
                                                        "    40  80 RED\n"
                                                        "    200 80 RED\n"));
            }
        }

        SECTION("it can insert a node to the right of the root") {
            REQUIRE(FERROR_OK == f_mrbt_insert_node(&tree, &expectedRight));
            REQUIRE_THAT(tree, MatchesTreeStructure("400 80 RED\n"
                                                    "  NIL\n"
                                                    "  800 80 RED\n"));

            SECTION("it can insert a node on lower levels") {
                MemoryRedBlackTreeNode otherLeft, otherRight;
                from_string("600 80 RED", otherLeft);
                from_string("C00 80 RED", otherRight);
                REQUIRE(FERROR_OK == f_mrbt_insert_node(&tree, &otherLeft));
                REQUIRE(FERROR_OK == f_mrbt_insert_node(&tree, &otherRight));
                REQUIRE_THAT(tree, MatchesTreeStructure("400 80 RED\n"
                                                        "  NIL\n"
                                                        "  800 80 RED\n"
                                                        "    600 80 RED\n"
                                                        "    C00 80 RED"));
            }
        }

        SECTION("it cannot insert a duplicate key") {
            MemoryRedBlackTreeNode copy = node;
            REQUIRE(FERROR_DUPLICATE_KEY == f_mrbt_insert_node(&tree, &copy));
        }
    }
}

TEST_CASE("Add ability to fix up nodes after an insert into a MemoryRedBlackTree") {
    auto tree = build_tree("100 40 RED");
    f_mrbt_insert_fixup(tree.get(), tree->root);
    REQUIRE_THAT(*tree, MatchesTreeStructure("100 40 BLACK"));

    tree = build_tree("100 40 BLACK\n"
                      "  50 20 RED");
    f_mrbt_insert_fixup(tree.get(), tree->head_block->nodes + 1);
    REQUIRE_THAT(*tree, MatchesTreeStructure("100 40 BLACK\n"
                                             "  50 20 RED"));

    SECTION("when the parent is red and is a left child of the grandparent and the uncle is also red") {
        tree = build_tree("100 40 BLACK\n"
                          "  50 20 RED\n"
                          "    25 10 RED\n"
                          "  150 200 RED\n"
        );
        f_mrbt_insert_fixup(tree.get(), tree->head_block->nodes + 2);
        REQUIRE_THAT(*tree, MatchesTreeStructure("100 40 BLACK\n"
                                                 "  50  20  BLACK\n"
                                                 "    25 10 RED\n"
                                                 "  150 200 BLACK\n"
        ));

        tree = build_tree("100 40 BLACK\n"
                          "  25  10  BLACK\n"
                          "    12 7  RED\n"
                          "      6 5 RED\n"
                          "    50 20 RED\n"
                          "  150 200 BLACK"
        );
        f_mrbt_insert_fixup(tree.get(), tree->head_block->nodes + 3);
        REQUIRE_THAT(*tree, MatchesTreeStructure("100 40 BLACK\n"
                                                 "  25  10  RED\n"
                                                 "    12 7  BLACK\n"
                                                 "      6 5 RED\n"
                                                 "    50 20 BLACK\n"
                                                 "  150 200 BLACK"
        ));
    }

    SECTION("when the parent is red and is a right child of the grandparent and the uncle is also red") {
        tree = build_tree("100 40 BLACK\n"
                          "  50 20 RED\n"
                          "  150 200 RED\n"
                          "    400 20 RED\n"
        );
        f_mrbt_insert_fixup(tree.get(), tree->head_block->nodes + 3);
        REQUIRE_THAT(*tree, MatchesTreeStructure("100 40 BLACK\n"
                                                 "  50  20  BLACK\n"
                                                 "  150 200 BLACK\n"
                                                 "    400 20 RED\n"
        ));
    }

    SECTION("When the parent is red and is a left child of the grandparent and the uncle is black") {
        tree = build_tree("100 40 BLACK\n"
                          "  50 20 RED\n"
                          "    25 10 RED"
                );
        f_mrbt_insert_fixup(tree.get(), tree->head_block->nodes + 2);
        REQUIRE_THAT(*tree, MatchesTreeStructure("50 20 BLACK\n"
                                                 "  25  10 RED\n"
                                                 "  100 40 RED"
        ));

        tree = build_tree("100 40 BLACK\n"
                          "  50 20 RED\n"
                          "    NIL\n"
                          "    75 10 RED"
        );
        f_mrbt_insert_fixup(tree.get(), tree->head_block->nodes + 2);
        REQUIRE_THAT(*tree, MatchesTreeStructure("75 10 BLACK\n"
                                                 "  50  20 RED\n"
                                                 "  100 40 RED"
        ));

        tree = build_tree("100 40 BLACK\n"
                          "  50  20  BLACK\n"
                          "    25 10 RED\n"
                          "      12 7 RED\n"
                          "  150 200 BLACK"
                );
        f_mrbt_insert_fixup(tree.get(), tree->head_block->nodes + 3);
        REQUIRE_THAT(*tree, MatchesTreeStructure("100 40 BLACK\n"
                                                 "  25  10  BLACK\n"
                                                 "    12 7  RED\n"
                                                 "    50 20 RED\n"
                                                 "  150 200 BLACK"
        ));

        tree = build_tree("100 40 BLACK\n"
                          "  50 20 BLACK\n"
                          "  150 200 BLACK\n"
                          "    140 10 RED\n"
                          "      120 15 RED"
        );
        f_mrbt_insert_fixup(tree.get(), tree->head_block->nodes + 4);
        REQUIRE_THAT(*tree, MatchesTreeStructure("100 40 BLACK\n"
                                                 "  50  20 BLACK\n"
                                                 "  140 10 BLACK\n"
                                                 "    120 15  RED\n"
                                                 "    150 200 RED"
        ));

        tree = build_tree("100 40 BLACK\n"
                          "  75 2 RED\n"
                          "    25 10 RED\n"
                          "      12 5  BLACK\n"
                          "        6 3 RED\n"
                          "      50 20 BLACK\n"
                          "    80 7  BLACK\n"
                          "  150 200 BLACK"
                );
        f_mrbt_insert_fixup(tree.get(), tree->head_block->nodes + 2);
        REQUIRE_THAT(*tree, MatchesTreeStructure("75 2 BLACK\n"
                                                 "  25 10 RED\n"
                                                 "    12 5  BLACK\n"
                                                 "      6 3 RED\n"
                                                 "    50 20 BLACK\n"
                                                 "  100 40 RED\n"
                                                 "    80 7 BLACK\n"
                                                 "    150 200 BLACK"
        ));

        tree = build_tree("100 40 BLACK\n"
                          "  75 2 RED\n"
                          "    25 10 BLACK\n"
                          "      12 5  RED\n"
                          "        6 3 RED\n"
                          "      50 20 RED\n"
                          "    80 7  BLACK\n"
                          "  150 200 BLACK"
        );
        f_mrbt_insert_fixup(tree.get(), tree->head_block->nodes + 4);
        REQUIRE_THAT(*tree, MatchesTreeStructure("75 2 BLACK\n"
                                                 "  25 10 RED\n"
                                                 "    12 5  BLACK\n"
                                                 "      6 3 RED\n"
                                                 "    50 20 BLACK\n"
                                                 "  100 40 RED\n"
                                                 "    80 7 BLACK\n"
                                                 "    150 200 BLACK"
        ));
    }

    SECTION("When the parent is red and is a right child of the grandparent and the uncle is black") {
        tree = build_tree("100 40 BLACK\n"
                          "  NIL\n"
                          "  150 20 RED\n"
                          "    NIL\n"
                          "    200 10 RED"
        );
        f_mrbt_insert_fixup(tree.get(), tree->head_block->nodes + 2);
        REQUIRE_THAT(*tree, MatchesTreeStructure("150 20 BLACK\n"
                                                 "  100 40 RED\n"
                                                 "  200 10 RED"
        ));

        tree = build_tree("100 40 BLACK\n"
                          "  NIL\n"
                          "  150 20 RED\n"
                          "    125 10 RED"
        );
        f_mrbt_insert_fixup(tree.get(), tree->head_block->nodes + 2);
        REQUIRE_THAT(*tree, MatchesTreeStructure("125 10 BLACK\n"
                                                 "  100 40 RED\n"
                                                 "  150 20 RED"
        ));

        tree = build_tree("100 40 BLACK\n"
                          "  50 20 BLACK\n"
                          "  150 200 BLACK\n"
                          "    NIL\n"
                          "    400 10 RED\n"
                          "      NIL\n"
                          "      800 100 RED"
                );
        f_mrbt_insert_fixup(tree.get(), tree->head_block->nodes + 4);
        REQUIRE_THAT(*tree, MatchesTreeStructure("100 40 BLACK\n"
                                                 "  50 20 BLACK\n"
                                                 "  400 10 BLACK\n"
                                                 "    150 200 RED\n"
                                                 "    800 100 RED"
        ));

        tree = build_tree("100 40 BLACK\n"
                          "  50  20  BLACK\n"
                          "    NIL\n"
                          "    75 2 RED\n"
                          "      NIL\n"
                          "      80 7 RED\n"
                          "  150 200 BLACK"
        );
        f_mrbt_insert_fixup(tree.get(), tree->head_block->nodes + 3);
        REQUIRE_THAT(*tree, MatchesTreeStructure("100 40 BLACK\n"
                                                 "  75  2  BLACK\n"
                                                 "    50 20  RED\n"
                                                 "    80 7  RED\n"
                                                 "  150 200 BLACK"
        ));

        tree = build_tree("100 40 BLACK\n"
                          "  50 20 BLACK\n"
                          "  140 10 RED\n"
                          "    120 15 BLACK\n"
                          "    400 10 RED\n"
                          "      150 200 BLACK\n"
                          "      800 7   BLACK\n"
                          "        NIL\n"
                          "        1000 2 RED"
                );
        f_mrbt_insert_fixup(tree.get(), tree->head_block->nodes + 4);
        REQUIRE_THAT(*tree, MatchesTreeStructure("140 10 BLACK\n"
                                                 "  100 40 RED\n"
                                                 "    50 20 BLACK\n"
                                                 "    120 15 BLACK\n"
                                                 "  400 10 RED\n"
                                                 "    150 200 BLACK\n"
                                                 "    800 7   BLACK\n"
                                                 "      NIL\n"
                                                 "      1000 2 RED"
        ));
    }
}

TEST_CASE("we can find the smallest node in a subtree") {
    auto tree = build_tree("140 10 BLACK\n"
                           "  100 40 RED\n"
                           "    50 20 BLACK\n"
                           "    120 15 BLACK\n"
                           "  400 10 RED\n"
                           "    150 200 BLACK\n"
                           "    800 7   BLACK\n"
                           "      NIL\n"
                           "      1000 2 RED");
    REQUIRE(tree->head_block->nodes + 2 == f_mrbtn_get_minimum(tree->root));
    REQUIRE(tree->head_block->nodes + 5 == f_mrbtn_get_minimum(tree->head_block->nodes + 4));
}

TEST_CASE("we can find the largest node in a subtree") {
    auto tree = build_tree("140 10 BLACK\n"
                           "  100 40 RED\n"
                           "    50 20 BLACK\n"
                           "    120 15 BLACK\n"
                           "  400 10 RED\n"
                           "    150 200 BLACK\n"
                           "    800 7   BLACK\n"
                           "      NIL\n"
                           "      1000 2 RED");
    REQUIRE(tree->head_block->nodes + 7 == f_mrbtn_get_maximum(tree->root));
    REQUIRE(tree->head_block->nodes + 3 == f_mrbtn_get_maximum(tree->head_block->nodes + 1));
}

TEST_CASE("get the successor for a node") {
    auto tree = build_tree("140 10 BLACK\n"
                           "  100 40 RED\n"
                           "    50 20 BLACK\n"
                           "    120 15 BLACK\n"
                           "  400 10 RED\n"
                           "    150 200 BLACK\n"
                           "    800 7   BLACK\n"
                           "      NIL\n"
                           "      1000 2 RED");
    auto nodes = tree->head_block->nodes;
    REQUIRE(nodes + 1 == f_mrbtn_get_successor(nodes + 2));
    REQUIRE(nodes + 5 == f_mrbtn_get_successor(nodes));
    REQUIRE(nodes == f_mrbtn_get_successor(nodes + 3));
    REQUIRE(nullptr == f_mrbtn_get_successor(nodes + 7));
}

TEST_CASE("get the predecessor for a node") {
    auto tree = build_tree("140 10 BLACK\n"
                           "  100 40 RED\n"
                           "    50 20 BLACK\n"
                           "    120 15 BLACK\n"
                           "  400 10 RED\n"
                           "    150 200 BLACK\n"
                           "    800 7   BLACK\n"
                           "      NIL\n"
                           "      1000 2 RED");
    auto nodes = tree->head_block->nodes;
    REQUIRE(nullptr == f_mrbtn_get_predecessor(nodes + 2));
    REQUIRE(nodes + 2 == f_mrbtn_get_predecessor(nodes + 1));
    REQUIRE(nodes + 3 == f_mrbtn_get_predecessor(nodes));
    REQUIRE(nodes == f_mrbtn_get_predecessor(nodes + 5));
}

TEST_CASE("a Red/Black tree can be traversed by an iterator") {
    auto tree = build_tree("140 10 BLACK\n"
                           "  100 40 RED\n"
                           "    50 20 BLACK\n"
                           "    120 15 BLACK\n"
                           "  400 10 RED\n"
                           "    150 200 BLACK\n"
                           "    800 7   BLACK\n"
                           "      NIL\n"
                           "      1000 2 RED");
    MemoryRedBlackTreeIterator iterator;
    REQUIRE(FERROR_OK == f_mrbt_get_iterator(tree.get(), &iterator));
    REQUIRE_THAT(*tree, HasIteration("50   20  BLACK\n"
                                     "100  40  RED\n"
                                     "120  15  BLACK\n"
                                     "140  10  BLACK\n"
                                     "150  200 BLACK\n"
                                     "400  10  RED\n"
                                     "800  7   BLACK\n"
                                     "1000 2   RED"
    ));
}

TEST_CASE("a Red/Black tree can be searched based on the given address") {
    auto tree = build_tree("140 F BLACK\n"
                           "  100 1F RED\n"
                           "    50 20 BLACK\n"
                           "    120 15 BLACK\n"
                           "  400 10 RED\n"
                           "    150 200 BLACK\n"
                           "    800 7   BLACK\n"
                           "      NIL\n"
                           "      1000 2 RED");
    auto nodes = tree->head_block->nodes;
    MemoryRedBlackTreeIterator iterator;

    REQUIRE(FERROR_ELEMENT_NOT_FOUND == f_mrbt_find_element(tree.get(), 0x2000, &iterator));

    REQUIRE(FERROR_OK == f_mrbt_find_element(tree.get(), 0x150, &iterator));
    REQUIRE(iterator.current - nodes == 5);

    REQUIRE(FERROR_OK == f_mrbt_find_element(tree.get(), 0x12C, &iterator));
    REQUIRE(iterator.current - nodes == 3);
}

TEST_CASE("A red/black tree can be iterated in reverse") {
    auto tree = build_tree("140 F BLACK\n"
                           "  100 1F RED\n"
                           "    50 20 BLACK\n"
                           "    120 15 BLACK\n"
                           "  400 10 RED\n"
                           "    150 200 BLACK\n"
                           "    800 7   BLACK\n"
                           "      NIL\n"
                           "      1000 2 RED");
    auto nodes = tree->head_block->nodes;
    MemoryRedBlackTreeIterator iterator;
    size_t offset;
    bool hasElements;

    REQUIRE(FERROR_OK == f_mrbt_find_element(tree.get(), 0x400, &iterator));
    iterator.forward = false;

    REQUIRE(FERROR_OK == f_mrbti_move_next(&iterator));
    REQUIRE(FERROR_OK == f_mrbti_has_elements(&iterator, &hasElements));
    REQUIRE(hasElements);
    REQUIRE(FERROR_OK == f_mrbti_get_offset(&iterator, &offset));
    REQUIRE(offset == 0x150);

    REQUIRE(FERROR_OK == f_mrbti_move_next(&iterator));
    REQUIRE(FERROR_OK == f_mrbti_has_elements(&iterator, &hasElements));
    REQUIRE(hasElements);
    REQUIRE(FERROR_OK == f_mrbti_get_offset(&iterator, &offset));
    REQUIRE(offset == 0x140);

    REQUIRE(FERROR_OK == f_mrbti_move_next(&iterator));
    REQUIRE(FERROR_OK == f_mrbti_has_elements(&iterator, &hasElements));
    REQUIRE(hasElements);
    REQUIRE(FERROR_OK == f_mrbti_get_offset(&iterator, &offset));
    REQUIRE(offset == 0x120);

    REQUIRE(FERROR_OK == f_mrbti_move_next(&iterator));
    REQUIRE(FERROR_OK == f_mrbti_has_elements(&iterator, &hasElements));
    REQUIRE(hasElements);
    REQUIRE(FERROR_OK == f_mrbti_get_offset(&iterator, &offset));
    REQUIRE(offset == 0x100);

    REQUIRE(FERROR_OK == f_mrbti_move_next(&iterator));
    REQUIRE(FERROR_OK == f_mrbti_has_elements(&iterator, &hasElements));
    REQUIRE(hasElements);
    REQUIRE(FERROR_OK == f_mrbti_get_offset(&iterator, &offset));
    REQUIRE(offset == 0x50);

    REQUIRE(FERROR_OK == f_mrbti_move_next(&iterator));
    REQUIRE(FERROR_OK == f_mrbti_has_elements(&iterator, &hasElements));
    REQUIRE_FALSE(hasElements);
}

TEST_CASE("A node can be transplanted onto the root node") {
    auto tree = build_tree("140 F BLACK\n"
                           "  100 1F RED\n"
                           "    50 20 BLACK\n"
                           "    120 15 BLACK\n"
                           "  400 10 RED\n"
                           "    150 200 BLACK\n"
                           "    800 7   BLACK\n"
                           "      NIL\n"
                           "      1000 2 RED");
    MemoryRedBlackTreeNode otherNode;
    from_string("130 F BLACK", otherNode);

    f_mrbt_transplant(tree.get(), tree->head_block->nodes, &otherNode);
    REQUIRE_THAT(*tree, MatchesTreeStructure("130 F BLACK"));

    f_mrbt_transplant(tree.get(), &otherNode, nullptr);
    REQUIRE(nullptr == tree->root);
}

TEST_CASE("A node can be transplanted onto a left child") {
    auto tree = build_tree("140 F BLACK\n"
                           "  100 1F RED\n"
                           "    50 20 BLACK\n"
                           "    120 15 BLACK\n"
                           "  400 10 RED\n"
                           "    150 200 BLACK\n"
                           "    800 7   BLACK\n"
                           "      NIL\n"
                           "      1000 2 RED");
    MemoryRedBlackTreeNode otherNode;
    from_string("75 F BLACK", otherNode);

    f_mrbt_transplant(tree.get(), tree->head_block->nodes + 2, &otherNode);
    REQUIRE_THAT(*tree, MatchesTreeStructure("140 F BLACK\n"
                                             "  100 1F RED\n"
                                             "    75  F  BLACK\n"
                                             "    120 15 BLACK\n"
                                             "  400 10 RED\n"
                                             "    150 200 BLACK\n"
                                             "    800 7   BLACK\n"
                                             "      NIL\n"
                                             "      1000 2 RED"));

    f_mrbt_transplant(tree.get(), &otherNode, nullptr);
    REQUIRE_THAT(*tree, MatchesTreeStructure("140 F BLACK\n"
                                             "  100 1F RED\n"
                                             "    NIL\n"
                                             "    120 15 BLACK\n"
                                             "  400 10 RED\n"
                                             "    150 200 BLACK\n"
                                             "    800 7   BLACK\n"
                                             "      NIL\n"
                                             "      1000 2 RED"));
}

TEST_CASE("A node can be transplanted onto a right child") {
    auto tree = build_tree("140 F BLACK\n"
                           "  100 1F RED\n"
                           "    50 20 BLACK\n"
                           "    120 15 BLACK\n"
                           "  400 10 RED\n"
                           "    150 200 BLACK\n"
                           "    800 7   BLACK\n"
                           "      NIL\n"
                           "      1000 2 RED");
    MemoryRedBlackTreeNode otherNode;
    from_string("110 F BLACK", otherNode);

    f_mrbt_transplant(tree.get(), tree->head_block->nodes + 3, &otherNode);
    REQUIRE_THAT(*tree, MatchesTreeStructure("140 F BLACK\n"
                                             "  100 1F RED\n"
                                             "    50  20 BLACK\n"
                                             "    110 F  BLACK\n"
                                             "  400 10 RED\n"
                                             "    150 200 BLACK\n"
                                             "    800 7   BLACK\n"
                                             "      NIL\n"
                                             "      1000 2 RED"));

    f_mrbt_transplant(tree.get(), &otherNode, nullptr);
    REQUIRE_THAT(*tree, MatchesTreeStructure("140 F BLACK\n"
                                             "  100 1F RED\n"
                                             "    50 20 BLACK\n"
                                             "  400 10 RED\n"
                                             "    150 200 BLACK\n"
                                             "    800 7   BLACK\n"
                                             "      NIL\n"
                                             "      1000 2 RED"));
}

TEST_CASE("A Red/Black Tree can have an element with only a left child deleted") {
    auto tree = build_tree("140 F BLACK\n"
                           "  100 1F RED\n"
                           "    50 20 BLACK\n"
                           "  400 10 RED\n"
                           "    150 200 BLACK\n"
                           "    800 7   BLACK\n"
                           "      NIL\n"
                           "      1000 2 RED"
            );

    MemoryRedBlackTreeNode* x;
    MemoryRedBlackTreeNode* xParent;
    MemoryRedBlackTreeColor yOriginalColor;
    f_mrbt_remove_node(tree.get(), tree->head_block->nodes + 1, &x, &xParent, &yOriginalColor);
    REQUIRE_THAT(*tree, MatchesTreeStructure("140 F BLACK\n"
                                             "  50 20 BLACK\n"
                                             "  400 10 RED\n"
                                             "    150 200 BLACK\n"
                                             "    800 7   BLACK\n"
                                             "      NIL\n"
                                             "      1000 2 RED"
    ));
    REQUIRE(x == tree->head_block->nodes + 2);
    REQUIRE(yOriginalColor == KMRBT_RED);
    REQUIRE(KMRBT_UNUSED == tree->head_block->nodes[1].color);
}

TEST_CASE("A Red/Black Tree can have an element with only a right child deleted") {
    auto tree = build_tree("140 F BLACK\n"
                           "  100 1F RED\n"
                           "    50 20 BLACK\n"
                           "  400 10 RED\n"
                           "    150 200 BLACK\n"
                           "    800 7   BLACK\n"
                           "      NIL\n"
                           "      1000 2 RED"
    );

    MemoryRedBlackTreeNode* x;
    MemoryRedBlackTreeNode* xParent;
    MemoryRedBlackTreeColor yOriginalColor;
    f_mrbt_remove_node(tree.get(), tree->head_block->nodes + 5, &x, &xParent, &yOriginalColor);
    REQUIRE_THAT(*tree, MatchesTreeStructure("140 F BLACK\n"
                                             "  100 1F RED\n"
                                             "    50 20 BLACK\n"
                                             "  400 10 RED\n"
                                             "    150  200 BLACK\n"
                                             "    1000 2   RED"
    ));
    REQUIRE(x == tree->head_block->nodes + 6);
    REQUIRE(yOriginalColor == KMRBT_BLACK);
    REQUIRE(KMRBT_UNUSED == tree->head_block->nodes[5].color);
}

TEST_CASE("A Red/Black Tree can remove a node with both left and right children") {
    auto tree = build_tree("140 F BLACK\n"
                           "  100 1F RED\n"
                           "    50 20 BLACK\n"
                           "    120 15 BLACK\n"
                           "  400 10 RED\n"
                           "    150 200 BLACK\n"
                           "    800 7   BLACK\n"
                           "      NIL\n"
                           "      1000 2 RED");

    MemoryRedBlackTreeNode* x;
    MemoryRedBlackTreeNode* xParent;
    MemoryRedBlackTreeColor yOriginalColor;
    f_mrbt_remove_node(tree.get(), tree->head_block->nodes + 1, &x, &xParent, &yOriginalColor);

    REQUIRE_THAT(*tree, MatchesTreeStructure("140 F BLACK\n"
                                        "  120 15 RED\n"
                                        "    50 20 BLACK\n"
                                        "  400 10 RED\n"
                                        "    150 200 BLACK\n"
                                        "    800 7   BLACK\n"
                                        "      NIL\n"
                                        "      1000 2 RED"));
    REQUIRE(x == nullptr);
    REQUIRE(yOriginalColor == KMRBT_BLACK);
}

/*
 * In red/black tree logic, we have the following algorithm for deletion:
 * - assume the node to be deleted is z
 * - find the smallest node in z's right subtree, call this node y
 * - call y's right node x
 * - copy y's key onto z, but keep z's color, then transplant x onto y
 *
 * In order to fixup the colors from this:
 * - if x is the root or x is red, then color x black and we're done
 * - call x's sibling w
 * - if w is x's right sibling:
 *   - CASE 1: if w is red, then transform into either cases 2-4:
 *     - color w black
 *     - color the parent of x red
 *     - left-rotate x's parent
 *     - repeat the whole fixup process
 *   - CASE 2: if w is black and both its children are black
 *     - color w red
 *     - color x's parent black
 *     - make x's parent the new x and repeat the whole fixup process
 *   - CASE 3: w is black and its left child is red
 *     - color w red
 *     - color w's left child black
 *     - right-rotate w
 *     - now x has a new w, and we are ready for CASE 4
 *   - CASE 4:
 *     - make w's color whatever color x's parent is
 *     - color x's parent black
 *     - color w's right child black
 *     - left rotate x's parent
 *     - now x has a new w, repeat the whole fixup process
 * - if w is x's left sibling:
 *   - CASE 1: if w is red, then transform into either cases 2-4:
 *     - color w black
 *     - color the parent of x red
 *     - right-rotate x's parent
 *     - repeat the whole fixup process
 *   - CASE 2: if w is black and both its children are black
 *     - color w red
 *     - color x's parent black
 *     - make x's parent the new x and repeat the whole fixup process
 *   - CASE 3: w is black and its right child is red
 *     - color w red
 *     - color w's right child black
 *     - left-rotate w
 *     - now x has a new w, and we are ready for CASE 4
 *   - CASE 4:
 *     - make w's color whatever color x's parent is
 *     - color x's parent black
 *     - color w's left child black
 *     - right rotate x's parent
 *     - now x has a new w, repeat the whole fixup process
 */
TEST_CASE("A Red/Black tree which has undergone a deletion can be fixed") {
    SECTION("the tree is now empty") {
        auto tree = build_tree("");
        REQUIRE(tree->root == NULL);
        f_mrbt_remove_fixup(tree.get(), tree->root, nullptr);
        REQUIRE_THAT(*tree, MatchesTreeStructure(""));
    }

    SECTION("can fixup a tree where x is the root node and the root is red") {
        auto tree = build_tree("100 10 RED");
        f_mrbt_remove_fixup(tree.get(), tree->root, nullptr);
        REQUIRE_THAT(*tree, MatchesTreeStructure("100 10 BLACK"));
    }

    SECTION("can fixup a tree where x is the root node and the root is black") {
        auto tree = build_tree("100 10 BLACK");
        f_mrbt_remove_fixup(tree.get(), tree->root, nullptr);
        REQUIRE_THAT(*tree, MatchesTreeStructure("100 10 BLACK"));
    }

    SECTION("can fixup a tree where x is red") {
        auto tree = build_tree("100 10 BLACK\n"
                               "  50  5 RED\n" // x
                               "  150 F BLACK"
                );
        f_mrbt_remove_fixup(tree.get(), tree->root->left, tree->root);
        REQUIRE_THAT(*tree, MatchesTreeStructure("100 10 BLACK\n"
                                                 "  50  5 BLACK\n"
                                                 "  150 F BLACK"
        ));
    }

    SECTION("x is the left child") {
        SECTION("CASE 1") {
            auto tree = build_tree("100 10 BLACK\n"
                                   "  50  10 BLACK\n"
                                   "    25 10 BLACK\n" // first x
                                   "    75 10 RED\n" // first w
                                   "      70 2 BLACK\n" // second w
                                   "      81 2 BLACK\n"
                                   "  150 10 BLACK"
                    );
            f_mrbt_remove_fixup(tree.get(), tree->head_block->nodes + 2, tree->head_block->nodes + 1);
            REQUIRE_THAT(*tree, MatchesTreeStructure("100 10 BLACK\n"
                                                     "  75  10 BLACK\n"
                                                     "    50 10 BLACK\n"
                                                     "      25 10 BLACK\n"
                                                     "      70 2 RED\n"
                                                     "    81 2 BLACK\n"
                                                     "  150 10 BLACK"
            ));
        }

        SECTION("CASE 2, where x's parent is the root and w is the right sibling and x is NIL and x's parent is root and w has only NULL children") {
            auto tree = build_tree("100 10 BLACK\n"
                                   "  NIL\n"     // x
                                   "  150 F BLACK" // w
            );
            f_mrbt_remove_fixup(tree.get(), tree->root->left, tree->root);
            REQUIRE_THAT(*tree, MatchesTreeStructure("100 10 BLACK\n"
                                                     "  NIL\n"
                                                     "  150 F RED"
            ));
        }

        SECTION("CASE 2, where x's parent is the root and w is the right sibling and x is NIL and x's parent is root and w has right NULL child and left BLACK child") {
            // This particular tree would never happen in real-life, but it serves to test this case in isolation
            auto tree = build_tree("100 10 BLACK\n"
                                   "  NIL\n"     // x
                                   "  150 F BLACK\n" // w
                                   "    120 10 BLACK"
            );
            f_mrbt_remove_fixup(tree.get(), tree->root->left, tree->root);
            REQUIRE_THAT(*tree, MatchesTreeStructure("100 10 BLACK\n"
                                                     "  NIL\n"
                                                     "  150 F RED\n"
                                                     "    120 10 BLACK"
            ));
        }

        SECTION("CASE 2, where x's parent is the root and w is the right sibling and x is NIL and x's parent is root and w has left NULL child and right BLACK child") {
            // This particular tree would never happen in real-life, but it serves to test this case in isolation
            auto tree = build_tree("100 10 BLACK\n"
                                   "  NIL\n"     // x
                                   "  150 F BLACK\n" // w
                                   "    NIL\n"
                                   "    120 10 BLACK"
            );
            f_mrbt_remove_fixup(tree.get(), tree->root->left, tree->root);
            REQUIRE_THAT(*tree, MatchesTreeStructure("100 10 BLACK\n"
                                                     "  NIL\n"
                                                     "  150 F RED\n"
                                                     "    NIL\n"
                                                     "    120 10 BLACK"
            ));
        }
    }

    SECTION("x is the right child") {
        SECTION("CASE 1") {
            auto tree = build_tree("100 10 BLACK\n"
                                   "  50  10 BLACK\n"
                                   "    25 10 RED\n" // first w
                                   "      10 5 BLACK\n"
                                   "      40 5 BLACK\n" // second w
                                   "    75 10 BLACK\n" // first x
                                   "  150 10 BLACK"
            );
            f_mrbt_remove_fixup(tree.get(), tree->head_block->nodes + 5, tree->head_block->nodes + 1);
            REQUIRE_THAT(*tree, MatchesTreeStructure("100 10 BLACK\n"
                                                     "  25  10 BLACK\n"
                                                     "    10 5 BLACK\n"
                                                     "    50 10 BLACK\n"
                                                     "      40 5  RED\n"
                                                     "      75 10 BLACK\n"
                                                     "  150 10 BLACK"
            ));
        }

        SECTION("CASE 2, where x's parent is the root and w is the left sibling and x is NIL and x's parent is root and w has only NULL children") {
            auto tree = build_tree("100 10 BLACK\n"
                                   "  150 F BLACK" // w
            );
            f_mrbt_remove_fixup(tree.get(), tree->root->right, tree->root);
            REQUIRE_THAT(*tree, MatchesTreeStructure("100 10 BLACK\n"
                                                     "  150 F RED"
            ));
        }

        SECTION("CASE 2, where x's parent is the root and w is the left sibling and x is NIL and x's parent is root and w has right NULL child and left BLACK child") {
            // This particular tree would never happen in real-life, but it serves to test this case in isolation
            auto tree = build_tree("100 10 BLACK\n"
                                   "  150 F BLACK\n" // w
                                   "    120 10 BLACK"
            );
            f_mrbt_remove_fixup(tree.get(), tree->root->right, tree->root);
            REQUIRE_THAT(*tree, MatchesTreeStructure("100 10 BLACK\n"
                                                     "  150 F RED\n"
                                                     "    120 10 BLACK"
            ));
        }

        SECTION("CASE 2, where x's parent is the root and w is the left sibling and x is NIL and x's parent is root and w has right NULL child and left BLACK child") {
            // This particular tree would never happen in real-life, but it serves to test this case in isolation
            auto tree = build_tree("100 10 BLACK\n"
                                   "  150 F BLACK\n" // w
                                   "    NIL\n"
                                   "    120 10 BLACK"
            );
            f_mrbt_remove_fixup(tree.get(), tree->root->right, tree->root);
            REQUIRE_THAT(*tree, MatchesTreeStructure("100 10 BLACK\n"
                                                     "  150 F RED\n"
                                                     "    NIL\n"
                                                     "    120 10 BLACK"
            ));
        }
    }

    SECTION("CASE 2, where it applies up the tree multiple times") {
        // This particular tree would never happen in real-life, but it serves to test this case in isolation
        // At least I put in the effort to make this example balanced :)
        auto tree = build_tree("100 F BLACK\n"
                               "  50  5  BLACK\n"  // second x
                               "    25 10 BLACK\n" // first w
                               "    75 10 BLACK\n" // first x
                               "  150 12 BLACK\n"  // second w
                               "    120 10 BLACK\n"
                               "    200 10 BLACK\n"
        );
        f_mrbt_remove_fixup(tree.get(), tree->head_block->nodes + 3, tree->head_block->nodes + 1);
        REQUIRE_THAT(*tree, MatchesTreeStructure("100 F BLACK\n"
                                                 "  50  5  BLACK\n"
                                                 "    25 10 RED\n"
                                                 "    75 10 BLACK\n"
                                                 "  150 12 RED\n"
                                                 "    120 10 BLACK\n"
                                                 "    200 10 BLACK\n"
        ));
    }
}

TEST_CASE("A Red/Black Tree used for memory") {
    SECTION("can not be initialized to a NULL") {
        FError error = f_mrbt_init(nullptr);
        CHECK(error == FERROR_NULL_REFERENCE);
    }

    SECTION("can be created") {
        MemoryRedBlackTree tree;
        FError error = f_mrbt_init(&tree);
        CHECK(error == FERROR_OK);

        REQUIRE_THAT(tree, HasAllocatedBlocks(""));
        REQUIRE_THAT(tree, HasIteration(""));
    }

    SECTION("does not add keys without allocated memory") {
        MemoryRedBlackTree tree;
        FError error = f_mrbt_init(&tree);
        REQUIRE(error == FERROR_OK);

        CHECK(FERROR_OUT_OF_MEMORY == f_mrbt_insert_element(&tree, 0x100, 1024UL));
    }
}

/*********************************************************************************************\
 *                                   Convenience Functions                                   *
\*********************************************************************************************/
namespace {
    inline MemoryRedBlackTreeNode* index_to_node_ptr(int index, MemoryRedBlackTreeNode* base);
}


#pragma region Convenience Function Tests
TEST_CASE("build_tree") {
    struct NodeDescriptor {
        int parentIndex;
        int leftIndex;
        int rightIndex;
        MemoryRedBlackTreeColor color;
        size_t offset;
        size_t length;
    };
    // @formatter:off
    NodeDescriptor nodeDescriptors[] = {
            // Parent Left Right Color        Offset Length
               -1,    1,   4,    KMRBT_BLACK, 75,    6,
               0,     2,   3,    KMRBT_RED,   25,    10,
               1,     -1,  -1,   KMRBT_BLACK, 12,    7,
               1,     -1,  -1,   KMRBT_BLACK, 50,    20,
               0,     5,   7,    KMRBT_RED,   100,   40,
               4,     -1,  6,    KMRBT_BLACK, 85,    4,
               5,     -1, -1,    KMRBT_RED,   90,    9,
               4,     -1, -1,    KMRBT_BLACK, 150,   200
    };
    // @formatter:on
    struct {
        MemoryRedBlackTreeNodeBlockBase base;
        MemoryRedBlackTreeNode nodes[8];
    } block;
    block.base.next = nullptr;
    block.base.length = 8UL;
    for (int i = 0; i < 8; ++i) {
        MemoryRedBlackTreeNode& node = block.nodes[i];
        NodeDescriptor& descriptor = nodeDescriptors[i];
        node.parent = index_to_node_ptr(descriptor.parentIndex, block.nodes);
        node.left = index_to_node_ptr(descriptor.leftIndex, block.nodes);
        node.right = index_to_node_ptr(descriptor.rightIndex, block.nodes);
        node.color = descriptor.color;
        node.offset = descriptor.offset;
        node.length = descriptor.length;
    }
    auto tree = build_tree("75 6 BLACK\n"
                           "  25 10 RED\n"
                           "    12 7 BLACK\n"
                           "    50 20 BLACK\n"
                           "  100 40 RED\n"
                           "    85 4 BLACK\n"
                           "      NIL\n"
                           "      90 9 RED\n"
                           "    150 200 BLACK\n");
    REQUIRE(tree->head_block != nullptr);
    REQUIRE(8UL == tree->head_block->length);
    auto nodes = tree->head_block->nodes;
    for (int i = 0; i < 8UL; ++i) {
        auto& node = nodes[i];
        auto& descriptor = nodeDescriptors[i];

        if (descriptor.parentIndex == -1) {
            REQUIRE(node.parent == nullptr);
        } else {
            REQUIRE(node.parent - nodes == descriptor.parentIndex);
        }

        if (descriptor.leftIndex == -1) {
            REQUIRE(node.left == nullptr);
        } else {
            REQUIRE(node.left - nodes == descriptor.leftIndex);
        }

        if (descriptor.rightIndex == -1) {
            REQUIRE(node.right == nullptr);
        } else {
            REQUIRE(node.right - nodes == descriptor.rightIndex);
        }
    }
}
#pragma endregion

#pragma region Convenience Functions
namespace {
    void destroy_node_block(MemoryRedBlackTreeNodeBlock* block) {
        if (block != nullptr) {
            block->~MemoryRedBlackTreeNodeBlock();
        }
    }

    inline MemoryRedBlackTreeNode* index_to_node_ptr(int index, MemoryRedBlackTreeNode* base) {
        return index < 0 ? nullptr : base + index;
    }
}
#pragma endregion
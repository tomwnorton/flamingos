//
// Created by thomas on 12/17/21.
//

#ifndef FLAMINGOS_RED_BLACK_TREE_MATCHERS_HXX
#define FLAMINGOS_RED_BLACK_TREE_MATCHERS_HXX

using std::uint8_t;
using std::istringstream;
using std::ostringstream;
using std::getline;
using std::string;

using namespace std::string_literals;

std::vector<string> get_lines(const string& str) {
    istringstream in(str);
    std::vector<string> result;
    while (!in.eof()) {
        string line;
        getline(in, line);
        if (line.find_first_not_of(" \t") != string::npos) {
            result.push_back(line);
        }
    }
    return result;
}

std::ostream& operator<<(std::ostream& out, const MemoryRedBlackTreeNode& node) {
    if (node.color == KMRBT_UNUSED) {
        out << "UNUSED";
    } else {
        out << std::hex << std::uppercase << node.offset
            << " " << node.length
            << " " << (node.color == KMRBT_BLACK ? "BLACK" : "RED");
    }
    return out;
}

std::istream& operator>>(std::istream& in, MemoryRedBlackTreeNode& node) {
    size_t offset, length;
    string color;

    node.parent = nullptr;
    node.left = nullptr;
    node.right = nullptr;

    in.clear();
    in >> std::hex >> offset >> length;
    if (in) {
        in >> color;
        if (in && color != "BLACK" && color != "RED") {
            in.setstate(std::ios::failbit);
        } else if (in) {
            node.offset = offset;
            node.length = length;
            if (color == "BLACK") {
                node.color = KMRBT_BLACK;
            } else {
                node.color = KMRBT_RED;
            }
        }
    } else {
        in.clear();
        in >> color;
        if (in && color != "UNUSED") {
            in.setstate(std::ios::failbit);
        } else {
            node.color = KMRBT_UNUSED;
        }
    }

    return in;
}

string trim(const string& s) {
    auto begin = s.find_first_not_of(" \t\r\n");
    auto end = s.find_last_not_of(" \t\r\n") + 1;
    if (begin == string::npos) return "";
    return s.substr(begin, end - begin);
}

string to_string(const MemoryRedBlackTreeNode& node) {
    ostringstream out;
    out << node;
    return out.str();
}

void from_string(const string& script, MemoryRedBlackTreeNode& node) {
    istringstream in(script);
    in >> node;
    if (!in) {
        throw std::invalid_argument("Could not parse node script \""s + script + "\"");
    }
}

void destroy_built_tree(MemoryRedBlackTree* tree) {
    auto buffer = reinterpret_cast<uint8_t*>(tree->head_block);
    delete [] buffer;
    delete tree;
}
typedef typeof(&destroy_built_tree) BuiltTreeDestructor;

std::shared_ptr<MemoryRedBlackTree> build_tree(const string& script) {
    auto lines = get_lines(script);
    auto tree = new MemoryRedBlackTree;

    if (script == "") {
        tree->head_block = new MemoryRedBlackTreeNodeBlock;
        tree->tail_block = tree->head_block;
        tree->head_block->length = 0;
        tree->head_block->next = nullptr;
        tree->root = nullptr;
        return std::shared_ptr<MemoryRedBlackTree>(tree, destroy_built_tree);
    }
    auto count = std::count_if(lines.begin(), lines.end(), [](const string& line) {return trim(line) != "NIL";});
    auto buffer = new uint8_t[sizeof(MemoryRedBlackTreeNodeBlock) + sizeof(MemoryRedBlackTreeNode) * count];
    tree->head_block = new (buffer) MemoryRedBlackTreeNodeBlock;
    tree->tail_block = tree->head_block;
    tree->head_block->length = count;
    tree->head_block->next = nullptr;

    MemoryRedBlackTreeNode NIL = {0};

    auto const INDENT_SIZE = 2;
    auto indentCount = 0;
    MemoryRedBlackTreeNode* parent = nullptr;
    for (auto i = 0, j = 0; i < lines.size(); ++i, ++j) {
        auto line = lines[i];
        auto& node = tree->head_block->nodes[j];
        auto leadingSpaceCount = line.find_first_not_of(' ');
        if (leadingSpaceCount % INDENT_SIZE != 0) {
            throw std::invalid_argument("Found incorrect indenting of "s + std::to_string(leadingSpaceCount) + " spaces");
        }
        auto indents = leadingSpaceCount / INDENT_SIZE;
        if (indents > indentCount) {
            throw std::logic_error(
                    "We somehow ended up with indents ("s
                    + std::to_string(indents)
                    + " ) > indentCount ("s
                    + std::to_string(indentCount)
                    + ")");
        }
        while (indentCount > indents) {
            if (parent == nullptr || parent->parent == nullptr) {
                throw std::invalid_argument("Invalid script given: \n"s + script);
            }
            --indentCount;
            parent = parent->parent;
        }

        if (trim(line) == "NIL") {
            if (parent->left == nullptr) {
                parent->left = &NIL;
            } else {
                parent->right = &NIL;
            }
            --j;
        } else {
            from_string(line, node);
            node.parent = parent;
            node.left = nullptr;
            node.right = nullptr;
            if (parent == nullptr) {
                tree->root = &node;
            } else if (parent->left == nullptr) {
                parent->left = &node;
            } else {
                parent->right = &node;
            }

            parent = &node;
            ++indentCount;
        }
    }

    for (auto pos = tree->head_block->nodes; pos != tree->head_block->nodes + tree->head_block->length; ++pos) {
        if (pos->parent == &NIL) pos->parent = nullptr;
        if (pos->left == &NIL) pos->left = nullptr;
        if (pos->right == &NIL) pos->right = nullptr;
    }
    return std::shared_ptr<MemoryRedBlackTree>(tree, destroy_built_tree);
}

void build_tree_script(const MemoryRedBlackTreeNode& node, std::ostream& out, int levels) {
    int const INDENT_LENGTH = 2;
    string indent(INDENT_LENGTH * levels, ' ');
    out << indent << node << "\n";
    if (node.left != nullptr) {
        build_tree_script(*node.left, out, levels + 1);
    } else if (node.right != nullptr) {
        out << indent << string(INDENT_LENGTH, ' ') << "NIL\n";
    }
    if (node.right != nullptr) build_tree_script(*node.right, out, levels + 1);
}

string build_tree_script(const MemoryRedBlackTree& tree) {
    ostringstream out;
    if (tree.root != nullptr) {
        build_tree_script(*tree.root, out, 0);
    }
    return out.str();
}

namespace Catch {
    template <>
    struct StringMaker<MemoryRedBlackTreeNode> {
        static string convert(const MemoryRedBlackTreeNode& value) {
            ostringstream out;
            out << value;
            return out.str();
        }
    };

    template <>
    struct StringMaker<MemoryRedBlackTree> {
        static string convert(const MemoryRedBlackTree& tree) {
            string baseIndent(2, ' ');
            ostringstream out;
            out << "Allocated Blocks:\n";
            int i = 0;
            auto block = tree.head_block;
            while (block != nullptr) {
                out << baseIndent << "Block " << i << ":\n";
                for (auto pos = block->nodes; pos != block->nodes + block->length; ++pos) {
                    out << baseIndent << baseIndent << *pos << "\n";
                }
                block = block->next;
            }
            out << "Tree Structure:\n";
            if (tree.root == nullptr) {
                out << "NIL\n";
            } else {
                build_tree_script(*tree.root, out, 1);
            }

            out << "Iteration:\n";
            MemoryRedBlackTreeIterator iterator;
            if (FERROR_OK != f_mrbt_get_iterator(const_cast<MemoryRedBlackTree*>(&tree), &iterator)) {
                FAIL("Could not create iterator");
            }
            bool hasElements;
            FError error = f_mrbti_has_elements(&iterator, &hasElements);
            if (FERROR_OK != error) {
                FAIL("Couldn't determine if there were any elements in the iterator");
            }
            while (hasElements) {
                size_t offset;
                size_t length;
                MemoryRedBlackTreeColor color;
                error = f_mrbti_get_offset(&iterator, &offset);
                if (FERROR_OK != error) {
                    out << "  Could not get the offset of the current element\n";
                    break;
                }

                error = f_mrbti_get_length(&iterator, &length);
                if (FERROR_OK != error) {
                    out << "  Could not get the length of the current element\n";
                    break;
                }

                error = f_mrbti_get_color(&iterator, &color);
                if (FERROR_OK != error) {
                    out << "  Could not get the color of the current element\n";
                    break;
                }

                out << "  " << std::hex << offset
                    << " "  << std::hex << length
                    << " "  << color
                    << std::endl;

                if (FERROR_OK != f_mrbti_move_next(&iterator)) {
                    out << "  Error moving to next element";
                    break;
                }

                if (FERROR_OK != f_mrbti_has_elements(&iterator, &hasElements)) {
                    out << "  Error determining if there are more elements\n";
                    break;
                }
            }

            return out.str();
        }
    };
}

namespace {

    void checf_allocations(const MemoryRedBlackTree& tree, const string& script);
    void checf_iteration(MemoryRedBlackTree& tree, const string& script);
    void checf_tree(const MemoryRedBlackTree& tree, const string& script);
    void destroy_node_block(MemoryRedBlackTreeNodeBlock* block);

    bool nodes_match(const MemoryRedBlackTreeNode& lhs, const MemoryRedBlackTreeNode& rhs) {
        return lhs.color == KMRBT_UNUSED && lhs.color == rhs.color
               || lhs.color == rhs.color
                  && lhs.offset == rhs.offset
                  && lhs.length == rhs.length;
    }

    class HasAllocatedBlocks : public Catch::MatcherBase<MemoryRedBlackTree> {
    public:
        explicit HasAllocatedBlocks(const string& script) : script(script) {
            auto lines = get_lines(script);
            for (auto line : lines) {
                MemoryRedBlackTreeNode node;
                from_string(line, node);
                expected.push_back(node);
            }
        }

        bool match(const MemoryRedBlackTree &actual) const override {
            auto expectedPos = expected.begin();
            auto block = actual.head_block;
            while (block != nullptr) {
                auto actualPos = block->nodes;
                auto actualEnd = block->nodes + block->length;
                for (; actualPos != actualEnd; ++actualPos, ++expectedPos) {
                    if (expectedPos == expected.end()) return false;
                    if (!nodes_match(*expectedPos, *actualPos)) return false;
                }
                block = block->next;
            }
            return block == nullptr;
        }

        string describe() const override {
            return "\n contains blocks that match \n"s + script;
        }

    private:
        string script;
        std::vector<MemoryRedBlackTreeNode> expected;
    };

    class HasIteration : public Catch::MatcherBase<MemoryRedBlackTree> {
    public:
        explicit HasIteration(const string& script) : script(script) {
            auto lines = get_lines(script);
            for (auto line : lines) {
                MemoryRedBlackTreeNode node;
                from_string(line, node);
                expected.push_back(node);
            }
        }

        bool match(const MemoryRedBlackTree& actual) const override {
            auto tree = const_cast<MemoryRedBlackTree*>(&actual);
            MemoryRedBlackTreeIterator iterator;
            if (FERROR_OK != f_mrbt_get_iterator(tree, &iterator)) {
                throw std::logic_error("Could not get iterator from tree");
            }
            bool hasElements = false;
            if (FERROR_OK != f_mrbti_has_elements(&iterator, &hasElements)) {
                throw std::logic_error("Could not call f_mrbti_has_elements");
            }
            auto pos = expected.begin();
            while (hasElements) {
                if (pos == expected.end()) {
                    return false;
                }
                MemoryRedBlackTreeNode node;
                if (FERROR_OK != f_mrbti_get_offset(&iterator, &node.offset)) {
                    throw std::logic_error("Could not call f_mrbti_get_offset");
                }
                if (FERROR_OK != f_mrbti_get_length(&iterator, &node.length)) {
                    throw std::logic_error("Could not call f_mrbti_get_length");
                }
                if (FERROR_OK != f_mrbti_get_color(&iterator, &node.color)) {
                    throw std::logic_error("Could not call f_mrbti_get_color");
                }
                if (!nodes_match(*pos, node)) {
                    return false;
                }

                ++pos;
                if (FERROR_OK != f_mrbti_move_next(&iterator)) {
                    throw std::logic_error("Could not call f_mrbti_move_next");
                }

                if (FERROR_OK != f_mrbti_has_elements(&iterator, &hasElements)) {
                    throw std::logic_error("Could not call f_mrbti_has_elements");
                }
            }
            return pos == expected.end();
        }

        string describe() const override {
            return "\n has its nodes iterated in this order \n"s + script;
        }

    private:
        string script;
        std::vector<MemoryRedBlackTreeNode> expected;
    };

    class MatchesTreeStructure : public Catch::MatcherBase<MemoryRedBlackTree> {
    public:
        explicit MatchesTreeStructure(const string& script)
            : script(script), expectedTree(build_tree(script)) {
        }

        bool match(const MemoryRedBlackTree& tree) const override {
            // The given script could have extra spaces added for readability,
            // so we can't use that as a point of comparison.
            // It's also MUCH easier to compare tree structures by looking at
            // the strings that have been generated.
            auto expected = build_tree_script(*expectedTree);
            auto actual = build_tree_script(tree);
            return expected == actual;
        }

        string describe() const override {
            return "\n is built with this tree structure \n"s + script;
        }
    private:
        string script;
        std::shared_ptr<MemoryRedBlackTree> expectedTree;
    };
}

#endif //FLAMINGOS_RED_BLACK_TREE_MATCHERS_HXX

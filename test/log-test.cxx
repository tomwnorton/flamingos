#include <cstring>
#include <string>
#include <vector>
#include <limits>
#include <catch2/catch.hpp>
#include "cxx.h"
#include "log.h"

using std::string;
using std::vector;
using namespace std::string_literals;

#define CALLBACK_WAS_CALLED REQUIRE_FALSE(g_logEntries.empty())
#define LAST_MESSAGE g_logEntries.back()
#define REQUIRE_LAST_MESSAGE(expected) REQUIRE(expected == LAST_MESSAGE)
#define ASSERT_LOG(logLevel, message, fileName, lineNumber, expected, ...) \
f_log((logLevel), (message), (fileName), (lineNumber) __VA_OPT__(,) __VA_ARGS__); \
REQUIRE_LAST_MESSAGE(expected)

string buffer;
vector<string> g_logEntries;

static int test_log(const char *s, int count, bool finished) {
    buffer += static_cast<string>(s).substr(0, count);
    if (finished) {
        g_logEntries.push_back(buffer);
        buffer = "";
    }
    return count;
}

TEST_CASE("The logger") {
    f_log_callback(test_log);

    SECTION("can log a message on a single-digit line") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message", "file.c", 0, "TRACE file.c (0):  The message");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message", "file.c", 1, "TRACE file.c (1):  The message");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message", "file.c", 2, "TRACE file.c (2):  The message");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message", "file.c", 3, "TRACE file.c (3):  The message");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message", "file.c", 4, "TRACE file.c (4):  The message");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message", "file.c", 5, "TRACE file.c (5):  The message");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message", "file.c", 6, "TRACE file.c (6):  The message");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message", "file.c", 7, "TRACE file.c (7):  The message");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message", "file.c", 8, "TRACE file.c (8):  The message");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message", "file.c", 9, "TRACE file.c (9):  The message");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message", "file.c", 10, "TRACE file.c (10):  The message");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message", "file.c", 11, "TRACE file.c (11):  The message");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message", "file.c", 198, "TRACE file.c (198):  The message");
        auto lineNumber = std::numeric_limits<uint32_t>::max();
        auto lineNumberStr = std::to_string(lineNumber);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message", "file.c", lineNumber, "TRACE file.c ("s + lineNumberStr + "):  The message");
    }

    SECTION("can log a message with different log levels") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message", "file.c", 1, "TRACE file.c (1):  The message");
        ASSERT_LOG(F_LOGLEVEL_DEBUG, "The message", "file.c", 1, "DEBUG file.c (1):  The message");
        ASSERT_LOG(F_LOGLEVEL_INFO,  "The message", "file.c", 1, "INFO  file.c (1):  The message");
        ASSERT_LOG(F_LOGLEVEL_WARN,  "The message", "file.c", 1, "WARN  file.c (1):  The message");
        ASSERT_LOG(F_LOGLEVEL_ERROR, "The message", "file.c", 1, "ERROR file.c (1):  The message");
        ASSERT_LOG(F_LOGLEVEL_FATAL, "The message", "file.c", 1, "FATAL file.c (1):  The message");
    }

    SECTION("can log a message formatted with a string") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %s", "file.c", 1, "TRACE file.c (1):  The message param", "param");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %s foo", "file.c", 1, "TRACE file.c (1):  The message param foo", "param");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %s then %s!", "file.c", 1, "TRACE file.c (1):  The message first then second!", "first", "second");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %", "file.c", 1, "TRACE file.c (1):  The message %");
    }

    SECTION("can log a message formatted with an unsigned decimal integer") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %u", "file.c", 1, "TRACE file.c (1):  The message 123", 123U);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %u", "file.c", 1, "TRACE file.c (1):  The message 4294967173", -123);
    }

    SECTION("can log a message formatted with an unsigned long decimal") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %lu", "file.c", 1, "TRACE file.c (1):  The message 123", 123UL);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %lu", "file.c", 1, "TRACE file.c (1):  The message 4294967173", -123L);
    }

    SECTION("can log a message formatted with an unsigned long long decimal") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %llu", "file.c", 1, "TRACE file.c (1):  The message 5000000000", 5000000000ULL);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %llu", "file.c", 1, "TRACE file.c (1):  The message 18446744073709551493", -123LL);
    }

    SECTION("can log a message formatted with a signed decimal integer") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %d", "file.c", 1, "TRACE file.c (1):  The message -123", -123);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %d", "file.c", 1, "TRACE file.c (1):  The message 123", 123);
    }

    SECTION("can log a message formatted with a signed long decimal") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %ld", "file.c", 1, "TRACE file.c (1):  The message -123", -123L);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %ld", "file.c", 1, "TRACE file.c (1):  The message 123", 123L);
    }

    SECTION("can log a message formatted with a signed long long decimal") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %lld", "file.c", 1, "TRACE file.c (1):  The message -123", -123LL);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %lld", "file.c", 1, "TRACE file.c (1):  The message 123", 123LL);
    }

    SECTION("can log a message formatted with an unsigned hexadecimal integer with lowercase letters") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %x", "file.c", 1, "TRACE file.c (1):  The message 7b", 123U);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %x", "file.c", 1, "TRACE file.c (1):  The message ffffff85", -123);
    }

    SECTION("can log a message formatted with an unsigned long hexadecimal integer with lowercase letters") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %lx", "file.c", 1, "TRACE file.c (1):  The message 7b", 123UL);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %lx", "file.c", 1, "TRACE file.c (1):  The message ffffff85", -123L);
    }

    SECTION("can log a message formatted with an unsigned long long hexadecimal integer with lowercase letters") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %llx", "file.c", 1, "TRACE file.c (1):  The message 123456789abcdef", 0x123456789ABCDEF);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %llx", "file.c", 1, "TRACE file.c (1):  The message ffffffffffffff85", -123LL);
    }

    SECTION("can log a message formatted with an unsigned hexadecimal integer with uppercase letters") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %X", "file.c", 1, "TRACE file.c (1):  The message 7B", 123U);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %X", "file.c", 1, "TRACE file.c (1):  The message FFFFFF85", -123);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %X", "file.c", 1, "TRACE file.c (1):  The message ABCD", 0xABCDU);
    }

    SECTION("can log a message formatted with an unsigned long hexadecimal integer with uppercase letters") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %lX", "file.c", 1, "TRACE file.c (1):  The message 7B", 123UL);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %lX", "file.c", 1, "TRACE file.c (1):  The message FFFFFF85", -123L);
    }

    SECTION("can log a message formatted with an unsigned long long hexadecimal integer with uppercase letters") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %llX", "file.c", 1, "TRACE file.c (1):  The message 123456789ABCDEF", 0x123456789ABCDEF);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message %llX", "file.c", 1, "TRACE file.c (1):  The message FFFFFFFFFFFFFF85", -123LL);
    }

    SECTION("can log a message padded to be right-aligned string") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%1s'", "file.c", 1, "TRACE file.c (1):  The message 'hi'", "hi");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%2s'", "file.c", 1, "TRACE file.c (1):  The message 'hi'", "hi");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%3s'", "file.c", 1, "TRACE file.c (1):  The message ' hi'", "hi");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%4s'", "file.c", 1, "TRACE file.c (1):  The message '  hi'", "hi");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%5s'", "file.c", 1, "TRACE file.c (1):  The message '   hi'", "hi");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%6s'", "file.c", 1, "TRACE file.c (1):  The message '    hi'", "hi");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%7s'", "file.c", 1, "TRACE file.c (1):  The message '     hi'", "hi");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%8s'", "file.c", 1, "TRACE file.c (1):  The message '      hi'", "hi");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%9s'", "file.c", 1, "TRACE file.c (1):  The message '       hi'", "hi");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%10s'", "file.c", 1, "TRACE file.c (1):  The message '        hi'", "hi");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%11s'", "file.c", 1, "TRACE file.c (1):  The message '         hi'", "hi");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%23s'", "file.c", 1, "TRACE file.c (1):  The message '                     hi'", "hi");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%123s'", "file.c", 1, "TRACE file.c (1):  The message '%123s'", "hi");
    }

    SECTION("can log a message padded to be right-aligned unsigned decimal") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%1u'", "file.c", 1, "TRACE file.c (1):  The message '9'", 9);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%2u'", "file.c", 1, "TRACE file.c (1):  The message ' 9'", 9);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%3u'", "file.c", 1, "TRACE file.c (1):  The message ' 89'", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%4u'", "file.c", 1, "TRACE file.c (1):  The message '  89'", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%5u'", "file.c", 1, "TRACE file.c (1):  The message '   89'", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%6u'", "file.c", 1, "TRACE file.c (1):  The message '    89'", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%7u'", "file.c", 1, "TRACE file.c (1):  The message '     89'", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%8u'", "file.c", 1, "TRACE file.c (1):  The message '      89'", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%9u'", "file.c", 1, "TRACE file.c (1):  The message '       89'", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%10u'", "file.c", 1, "TRACE file.c (1):  The message '        89'", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%11u'", "file.c", 1, "TRACE file.c (1):  The message '         89'", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%23u'", "file.c", 1, "TRACE file.c (1):  The message '                     89'", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%123u'", "file.c", 1, "TRACE file.c (1):  The message '%123u'", 89);
    }

    SECTION("can log a message padded to be right-aligned signed decimal") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%1d'", "file.c", 1, "TRACE file.c (1):  The message '9'", 9);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%2d'", "file.c", 1, "TRACE file.c (1):  The message ' 9'", 9);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%3d'", "file.c", 1, "TRACE file.c (1):  The message ' -9'", -9);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%4d'", "file.c", 1, "TRACE file.c (1):  The message ' -89'", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%5d'", "file.c", 1, "TRACE file.c (1):  The message '  -89'", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%6d'", "file.c", 1, "TRACE file.c (1):  The message '   -89'", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%7d'", "file.c", 1, "TRACE file.c (1):  The message '    -89'", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%8d'", "file.c", 1, "TRACE file.c (1):  The message '     -89'", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%9d'", "file.c", 1, "TRACE file.c (1):  The message '      -89'", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%10d'", "file.c", 1, "TRACE file.c (1):  The message '       -89'", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%11d'", "file.c", 1, "TRACE file.c (1):  The message '        -89'", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%23d'", "file.c", 1, "TRACE file.c (1):  The message '                    -89'", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%123d'", "file.c", 1, "TRACE file.c (1):  The message '%123d'", -89);
    }

    SECTION("can log a message with an unsigned decimal padded with zeroes instead of spaces") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%01u'", "file.c", 1, "TRACE file.c (1):  The message '9'", 9);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%02u'", "file.c", 1, "TRACE file.c (1):  The message '09'", 9);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%03u'", "file.c", 1, "TRACE file.c (1):  The message '089'", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%04u'", "file.c", 1, "TRACE file.c (1):  The message '0089'", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%05u'", "file.c", 1, "TRACE file.c (1):  The message '00089'", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%06u'", "file.c", 1, "TRACE file.c (1):  The message '000089'", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%07u'", "file.c", 1, "TRACE file.c (1):  The message '0000089'", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%08u'", "file.c", 1, "TRACE file.c (1):  The message '00000089'", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%09u'", "file.c", 1, "TRACE file.c (1):  The message '000000089'", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%010u'", "file.c", 1, "TRACE file.c (1):  The message '0000000089'", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%011u'", "file.c", 1, "TRACE file.c (1):  The message '00000000089'", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%023u'", "file.c", 1, "TRACE file.c (1):  The message '00000000000000000000089'", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%0123u'", "file.c", 1, "TRACE file.c (1):  The message '%0123u'", 89);
    }

    SECTION("can log a message with an unsigned decimal padded with zeroes instead of spaces") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%01X'", "file.c", 1, "TRACE file.c (1):  The message '9'", 0x9);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%02X'", "file.c", 1, "TRACE file.c (1):  The message '09'", 0x9);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%03X'", "file.c", 1, "TRACE file.c (1):  The message '089'", 0x89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%04X'", "file.c", 1, "TRACE file.c (1):  The message '0089'", 0x89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%05X'", "file.c", 1, "TRACE file.c (1):  The message '00089'", 0x89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%06X'", "file.c", 1, "TRACE file.c (1):  The message '000089'", 0x89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%07X'", "file.c", 1, "TRACE file.c (1):  The message '0000089'", 0x89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%08X'", "file.c", 1, "TRACE file.c (1):  The message '00000089'", 0x89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%09X'", "file.c", 1, "TRACE file.c (1):  The message '000000089'", 0x89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%010X'", "file.c", 1, "TRACE file.c (1):  The message '0000000089'", 0x89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%011X'", "file.c", 1, "TRACE file.c (1):  The message '00000000089'", 0x89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%023X'", "file.c", 1, "TRACE file.c (1):  The message '00000000000000000000089'", 0x89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%0123X'", "file.c", 1, "TRACE file.c (1):  The message '%0123X'", 0x89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%08X%08X'", "file.c", 1, "TRACE file.c (1):  The message '0000ABCD01234567'", 0xABCDUL, 0x1234567UL);
    }

    SECTION("can log a message with a signed decimal padded with 0s") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%01d'", "file.c", 1, "TRACE file.c (1):  The message '9'", 9);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%02d'", "file.c", 1, "TRACE file.c (1):  The message '09'", 9);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%03d'", "file.c", 1, "TRACE file.c (1):  The message '-09'", -9);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%04d'", "file.c", 1, "TRACE file.c (1):  The message '-089'", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%05d'", "file.c", 1, "TRACE file.c (1):  The message '-0089'", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%06d'", "file.c", 1, "TRACE file.c (1):  The message '-00089'", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%07d'", "file.c", 1, "TRACE file.c (1):  The message '-000089'", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%08d'", "file.c", 1, "TRACE file.c (1):  The message '-0000089'", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%09d'", "file.c", 1, "TRACE file.c (1):  The message '-00000089'", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%010d'", "file.c", 1, "TRACE file.c (1):  The message '-000000089'", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%011d'", "file.c", 1, "TRACE file.c (1):  The message '-0000000089'", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%023d'", "file.c", 1, "TRACE file.c (1):  The message '-0000000000000000000089'", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%0123d'", "file.c", 1, "TRACE file.c (1):  The message '%0123d'", -89);
    }

    SECTION("can log a message padded to be left-aligned string") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-1s'", "file.c", 1, "TRACE file.c (1):  The message 'hi'", "hi");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-2s'", "file.c", 1, "TRACE file.c (1):  The message 'hi'", "hi");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-3s'", "file.c", 1, "TRACE file.c (1):  The message 'hi '", "hi");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-4s'", "file.c", 1, "TRACE file.c (1):  The message 'hi  '", "hi");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-5s'", "file.c", 1, "TRACE file.c (1):  The message 'hi   '", "hi");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-6s'", "file.c", 1, "TRACE file.c (1):  The message 'hi    '", "hi");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-7s'", "file.c", 1, "TRACE file.c (1):  The message 'hi     '", "hi");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-8s'", "file.c", 1, "TRACE file.c (1):  The message 'hi      '", "hi");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-9s'", "file.c", 1, "TRACE file.c (1):  The message 'hi       '", "hi");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-10s'", "file.c", 1, "TRACE file.c (1):  The message 'hi        '", "hi");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-11s'", "file.c", 1, "TRACE file.c (1):  The message 'hi         '", "hi");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-23s'", "file.c", 1, "TRACE file.c (1):  The message 'hi                     '", "hi");
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-123s'", "file.c", 1, "TRACE file.c (1):  The message '%-123s'", "hi");
    }

    SECTION("can log a message padded to be left-aligned unsigned decimal") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-1u'", "file.c", 1, "TRACE file.c (1):  The message '9'", 9);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-2u'", "file.c", 1, "TRACE file.c (1):  The message '9 '", 9);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-3u'", "file.c", 1, "TRACE file.c (1):  The message '89 '", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-4u'", "file.c", 1, "TRACE file.c (1):  The message '89  '", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-5u'", "file.c", 1, "TRACE file.c (1):  The message '89   '", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-6u'", "file.c", 1, "TRACE file.c (1):  The message '89    '", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-7u'", "file.c", 1, "TRACE file.c (1):  The message '89     '", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-8u'", "file.c", 1, "TRACE file.c (1):  The message '89      '", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-9u'", "file.c", 1, "TRACE file.c (1):  The message '89       '", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-10u'", "file.c", 1, "TRACE file.c (1):  The message '89        '", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-11u'", "file.c", 1, "TRACE file.c (1):  The message '89         '", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-23u'", "file.c", 1, "TRACE file.c (1):  The message '89                     '", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-123u'", "file.c", 1, "TRACE file.c (1):  The message '%-123u'", 89);
    }

    SECTION("ignores the 0 padding character for left alignment") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-01u'", "file.c", 1, "TRACE file.c (1):  The message '9'", 9);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-02u'", "file.c", 1, "TRACE file.c (1):  The message '9 '", 9);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-03u'", "file.c", 1, "TRACE file.c (1):  The message '89 '", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-04u'", "file.c", 1, "TRACE file.c (1):  The message '89  '", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-05u'", "file.c", 1, "TRACE file.c (1):  The message '89   '", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-06u'", "file.c", 1, "TRACE file.c (1):  The message '89    '", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-07u'", "file.c", 1, "TRACE file.c (1):  The message '89     '", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-08u'", "file.c", 1, "TRACE file.c (1):  The message '89      '", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-09u'", "file.c", 1, "TRACE file.c (1):  The message '89       '", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-010u'", "file.c", 1, "TRACE file.c (1):  The message '89        '", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-011u'", "file.c", 1, "TRACE file.c (1):  The message '89         '", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-023u'", "file.c", 1, "TRACE file.c (1):  The message '89                     '", 89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-0123u'", "file.c", 1, "TRACE file.c (1):  The message '%-0123u'", 89);
    }

    SECTION("can log a message padded to be left-aligned signed decimal") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-1d'", "file.c", 1, "TRACE file.c (1):  The message '9'", 9);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-2d'", "file.c", 1, "TRACE file.c (1):  The message '9 '", 9);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-3d'", "file.c", 1, "TRACE file.c (1):  The message '-9 '", -9);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-4d'", "file.c", 1, "TRACE file.c (1):  The message '-89 '", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-5d'", "file.c", 1, "TRACE file.c (1):  The message '-89  '", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-6d'", "file.c", 1, "TRACE file.c (1):  The message '-89   '", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-7d'", "file.c", 1, "TRACE file.c (1):  The message '-89    '", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-8d'", "file.c", 1, "TRACE file.c (1):  The message '-89     '", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-9d'", "file.c", 1, "TRACE file.c (1):  The message '-89      '", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-10d'", "file.c", 1, "TRACE file.c (1):  The message '-89       '", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-11d'", "file.c", 1, "TRACE file.c (1):  The message '-89        '", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-23d'", "file.c", 1, "TRACE file.c (1):  The message '-89                    '", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-123d'", "file.c", 1, "TRACE file.c (1):  The message '%-123d'", -89);
    }

    SECTION("ignores 0 for left-aligned signed decimal") {
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-01d'", "file.c", 1, "TRACE file.c (1):  The message '9'", 9);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-02d'", "file.c", 1, "TRACE file.c (1):  The message '9 '", 9);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-03d'", "file.c", 1, "TRACE file.c (1):  The message '-9 '", -9);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-04d'", "file.c", 1, "TRACE file.c (1):  The message '-89 '", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-05d'", "file.c", 1, "TRACE file.c (1):  The message '-89  '", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-06d'", "file.c", 1, "TRACE file.c (1):  The message '-89   '", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-07d'", "file.c", 1, "TRACE file.c (1):  The message '-89    '", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-08d'", "file.c", 1, "TRACE file.c (1):  The message '-89     '", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-09d'", "file.c", 1, "TRACE file.c (1):  The message '-89      '", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-010d'", "file.c", 1, "TRACE file.c (1):  The message '-89       '", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-011d'", "file.c", 1, "TRACE file.c (1):  The message '-89        '", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-023d'", "file.c", 1, "TRACE file.c (1):  The message '-89                    '", -89);
        ASSERT_LOG(F_LOGLEVEL_TRACE, "The message '%-0123d'", "file.c", 1, "TRACE file.c (1):  The message '%-0123d'", -89);
    }
}
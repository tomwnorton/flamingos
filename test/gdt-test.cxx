#include <algorithm>
#include <cstdint>
#include <vector>
#include <catch2/catch.hpp>
#include "gdt.h"

using std::uint8_t;
using std::vector;
using std::copy;

TEST_CASE("The GDT can be built from a model") {
    GDTEntryModel models[] = {
            0,             0,          GDT_SEGMENT_TYPE_NULL,              0,
            0x12'34'56'78, 0x09'AB'CD, GDT_SEGMENT_TYPE_CODE_EXECUTE_READ, GDT_GRANULARITY_BYTES,
            0x11'11'11'11, 0x0F'FF'FF, GDT_SEGMENT_TYPE_CODE_EXECUTE_ONLY, GDT_GRANULARITY_PAGES,
            0x11'11'11'11, 0x0F'FF'FF, GDT_SEGMENT_TYPE_DATA_READ_WRITE,   GDT_GRANULARITY_BYTES,
            0x11'11'11'11, 0x0F'FF'FF, GDT_SEGMENT_TYPE_DATA_READ_ONLY,    GDT_GRANULARITY_BYTES,
            0x11'11'11'11, 0x0F'FF'FF, GDT_SEGMENT_TYPE_TASK_STATE,        GDT_GRANULARITY_BYTES
    };

    uint8_t expectedAll[][8] = {
            { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },
            { 0xCD, 0xAB, 0x78, 0x56, 0x34, 0x9A, 0x49, 0x12 },
            { 0xFF, 0xFF, 0x11, 0x11, 0x11, 0X98, 0xCF, 0x11 },
            { 0xFF, 0xFF, 0x11, 0x11, 0x11, 0x92, 0x4F, 0x11 },
            { 0xFF, 0xFF, 0x11, 0x11, 0x11, 0x90, 0x4F, 0x11 },
            { 0xFF, 0xFF, 0x11, 0x11, 0x11, 0x89, 0x4F, 0x11 }
    };
    const size_t expectedCount = sizeof(expectedAll) / sizeof(expectedAll[0]);
    GDTEntry entries[expectedCount];
    REQUIRE(f_populate_gdt_entries(models, models + expectedCount, entries));

    for (auto i = 0UL; i < expectedCount; ++i) {
        auto expectedArray = expectedAll[i];
        auto actualArray = reinterpret_cast<uint8_t*>(entries + i);
        vector<uint8_t> expected(8UL);
        vector<uint8_t> actual(8UL);
        copy(expectedArray, expectedArray + 8UL, expected.begin());
        copy(actualArray, actualArray + 8UL, actual.begin());
        REQUIRE(expected == actual);
    }
}

extern "C" void f_load_gdt(struct GDTEntry *entries, uint16_t sizeInBytes) {
    //
}

extern "C" void f_reload_segments() {
    //
}

TEST_CASE("The GDT cannot be built if it is invalid") {
    GDTEntryModel models[] = {
            0,             0,          GDT_SEGMENT_TYPE_NULL,              0,
            0x12'34'56'78, 0x19'AB'CD, GDT_SEGMENT_TYPE_CODE_EXECUTE_READ, GDT_GRANULARITY_BYTES,
            0x11'11'11'11, 0x0F'FF'FF, GDT_SEGMENT_TYPE_CODE_EXECUTE_ONLY, GDT_GRANULARITY_PAGES,
            0x11'11'11'11, 0x0F'FF'FF, GDT_SEGMENT_TYPE_DATA_READ_WRITE,   GDT_GRANULARITY_BYTES,
            0x11'11'11'11, 0x0F'FF'FF, GDT_SEGMENT_TYPE_DATA_READ_ONLY,    GDT_GRANULARITY_BYTES,
            0x11'11'11'11, 0x0F'FF'FF, GDT_SEGMENT_TYPE_TASK_STATE,        GDT_GRANULARITY_BYTES
    };
    const size_t count = sizeof(models) / sizeof(models[0]);
    GDTEntry entries[count];
    REQUIRE_FALSE(f_populate_gdt_entries(models, models + count, entries));
}
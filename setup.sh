#!/usr/bin/env bash
if command -v apt-get > /dev/null 2>&1; then
    HOST_OS=Debian
elif command -v dnf > /dev/null 2>&1 ; then
    HOST_OS=Fedora
else
    >&2 echo "Your OS is not supported as a development OS"
    exit 1
fi

# Double-check that the required packages are installed
if [ "$HOST_OS" == "Debian" ] ; then
    sudo apt-get install build-essential bison flex libgmp3-dev libmpc-dev libmpfr-dev texinfo
else
    sudo dnf install gcc gcc-c++ make bison flex gmp-devel libmpc-devel mpfr-devel texinfo
fi


#######################################################################################################################
# Install the cross-compiler toolchain
#######################################################################################################################
mkdir -p third-party
cd third-party
mkdir -p build-binutils
mkdir -p build-gcc
export PREFIX="/usr/local"
export TARGET=x86_64-elf
BINUTILS_BASE=binutils-2.34
GCC_BASE=gcc-11.2.0


if ! command -v x86_64-elf-ar > /dev/null 2>&1 ; then
    if [ ! -d $BINUTILS_BASE ] ; then
        if [ ! -f $BINUTILS_BASE.tar.xz ] ; then
            if ! wget https://ftp.gnu.org/gnu/binutils/$BINUTILS_BASE.tar.xz ; then
                >&2 echo "Could not download $BINUTILS_BASE.tar.xz"
                exit 1
            fi
        fi

        if ! tar -xJvf $BINUTILS_BASE.tar.xz ; then
            >&2 echo "Could not extract $BINUTILS_BASE.tar.xz"
            exit 1
        fi
    fi

    cd build-binutils
    rm -rf *
    if ! ../$BINUTILS_BASE/configure --target=$TARGET --prefix="$PREFIX" --with-sysroot --disable-nls --disable-werror ; then
        >&2 echo "Could not configure $BINUTILS_BASE"
        >&2 pwd
        exit 1
    fi
    if ! make ; then
        >&2 echo "Could not make $BINUTILS_BASE"
        exit 1
    fi
    if ! sudo make install ; then
        >&2 echo "Could not install $BINUTILS_BASE"
        exit 1
    fi
    cd ..
    rm -rf build-binutils
fi

if ! command -v x86_64-elf-gcc > /dev/null 2>&1 ; then
    if [ ! -d $GCC_BASE ] ; then
        if [ ! -f $GCC_BASE.tar.xz ] ; then
            if ! wget https://ftp.gnu.org/gnu/gcc/$GCC_BASE/$GCC_BASE.tar.xz ; then
                >&2 echo "Could not download $GCC_BASE.tar.xz"
                exit 1
            fi
        fi

        if ! tar -xJvf $GCC_BASE.tar.xz ; then
            >&2 echo "Could not extract $GCC_BASE.tar.xz"
            exit 1
        fi
    fi

    # Remove red zone support from ligbcc
    >&2 echo "Create t-x86_64-elf config file"
    RED_ZONE_CONFIG=$GCC_BASE/gcc/config/t-x86_64-elf
    touch $RED_ZONE_CONFIG
    echo "# Add libgcc multilib variant without red-zone requirement" > $RED_ZONE_CONFIG
    echo >> $RED_ZONE_CONFIG
    echo "MULTILIB_OPTIONS += mno-red-zone" >> $RED_ZONE_CONFIG
    echo "MULTILIB_DIRNAMES += no-red-zone" >> $RED_ZONE_CONFIG

    >&2 echo "Update config.gcc file"
        GCC_CONFIG_FILE=$GCC_BASE/gcc/config.gcc
    if ! grep --line-number 'tm_file="${tm_file} i386/unix.h i386/att.h dbxelf.h elfos.h newlib-stdint.h i386/i386elf.h i386/x86-64.h"' $GCC_CONFIG_FILE | grep -E 'x86_46-\*-elf' ; then
        LINES_IN_GCC_CONFIG_FILE=$(wc --lines $GCC_CONFIG_FILE | sed -E 's/([0-9]+).*/\1/')
        LINE_TO_INSERT_BEFORE=$(grep --line-number 'tm_file="${tm_file} i386/unix.h i386/att.h dbxelf.h elfos.h newlib-stdint.h i386/i386elf.h i386/x86-64.h"' $GCC_CONFIG_FILE | sed -E 's/^([0-9]+).*/\1/')
        LINES_BEFORE_INSERT=$((LINE_TO_INSERT_BEFORE - 1))
        LINES_AFTER_INSERT=$((LINES_IN_GCC_CONFIG_FILE - LINES_BEFORE_INSERT))
        >&2 echo "Make a tmp file"
        TMP_FILE=$(mktemp)
        >&2 echo "Write to the tmp file"
        head -n${LINES_BEFORE_INSERT} $GCC_CONFIG_FILE > $TMP_FILE
        echo '    tmake_file="${tmake_file} i386/t-x86_64-elf" # include the new multilib configuration' >> $TMP_FILE
        tail -n${LINES_AFTER_INSERT} $GCC_CONFIG_FILE >> $TMP_FILE
        >&2 echo "Overwrite the config.gcc file"
        cp $TMP_FILE $GCC_CONFIG_FILE
        >&2 echo "Delete teh tmp file"
        rm $TMP_FILE
    fi

    cd build-gcc
    rm -rf *
    if ! ../$GCC_BASE/configure --target=$TARGET --prefix="$PREFIX" --disable-nls --enable-languages=c,c++ --without-headers --disable-bootstrap ; then
        >&2 echo "Could not configure $GCC_BASE"
        exit 1
    fi
    if ! make all-gcc; then
        >&2 echo "Could not make $GCC_BASE"
        exit 1
    fi
    if ! make all-target-libgcc; then
        >&2 echo "Could not make libgcc for $GCC_BASE"
        exit 1
    fi
    if ! sudo make install-gcc ; then
        >&2 echo "Could not install $GCC_BASE"
        exit 1
    fi
    if ! sudo make install-target-libgcc ; then
        >&2 echo "Could not install $GCC_BASE"
        exit 1
    fi
    
    cd ..
    rm -rf build-gcc
fi

#######################################################################################################################
# Install Catch2
#######################################################################################################################
git clone --branch v2.13.7 -depth 1 https://github.com/catchorg/Catch2.git